import numpy as np
import sys

from typing import Iterable

sys.path.append('../')
from stochastic.stochasticprocess import WienerProcess

W = WienerProcess(N=2, dim=2, seed=12561646)


def three_point_dist(h: float, m=2) -> Iterable[float]:
    """С вероятностями 1/6, 2/3, 1/6 выпадают числа
    -sqrt(3h), 0, sqrt(3h)"""
    np.random.seed()
    R = np.random.random(m)
    res = []
    for r in R:
        if 0 <= r < 1.0/6.0:
            res.append(-np.sqrt(3*h))
        elif 1.0/6.0 <= r < 2.0/3.0:
            res.append(0.0)
        elif 2.0/3.0 <= r < 1.0:
            res.append(np.sqrt(3*h))
    return np.array(res)


def two_point_dist(h: float, m=2) -> Iterable[float]:
    """С вероятностями 1/2 и 1/2 выпадают числа
    -sqrt(h) и sqrt(h)"""
    np.random.seed()
    R = np.random.random(m)
    res = []
    for r in R:
        if 0 <= r < 1.0/2.0:
            res.append(-np.sqrt(h))
        else:
            res.append(np.sqrt(h))
    return np.array(res)



import timeit
import cProfile

cProfile.run('three_point_dist(0.1, m=20)')
cProfile.run('two_point_dist(0.1, m=20)')

print(timeit.timeit('two_point_dist(h=0.1, m=20)', number=100, globals=globals()))
print(timeit.timeit('three_point_dist(h=0.1, m=20)', number=100, globals=globals()))
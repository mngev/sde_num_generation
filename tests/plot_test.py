#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import sys

sys.path.append('../')
from stochastic import stochasticprocess as sp

plt.style.use('../../../iPyNotebook/default_colors.mplstyle')

W_1 = sp.WienerProcess(time_interval=(0.0, 5.0), h=0.002)
W_2 = sp.WienerProcess(time_interval=(0.0, 5.0), h=0.02)
W_3 = sp.WienerProcess(time_interval=(0.0, 5.0), N=1000)

poi_1 = sp.PoissonProcess(Lambda=10.0, N=50, time_interval=(0, 50))
poi_2 = sp.PoissonProcess(Lambda=20.0, N=50, time_interval=(0, 50))
poi_3 = sp.PoissonProcess(Lambda=30.0, N=50, time_interval=(0, 50))


# Протестируем процесс Винера
fig01 = plt.figure(1)
ax01 = fig01.add_subplot(1, 1, 1)

for i, W in enumerate((W_1, W_2, W_3)):
    ax01.plot(W.t, W.x, lw=0.7, label=r'$W_{0}(t)$'.format(i+1))

ax01.set_ylabel(r"$W_{t}$")
ax01.set_xlabel(r"$t$")

ax01.legend(loc='lower left', ncol=3, framealpha=0.5)

fig01.savefig('wiener_process.png', format='png', dpi=400, bbox_inches='tight',  pad_inches=0)


# Протестируем процесс Пуассона
fig02 = plt.figure(2)
ax02 = fig02.add_subplot(1, 1, 1)

for P in (poi_1, poi_2, poi_3):
    ax02.step(P.t, P.x, label=r"$\lambda = {0:.2g}$".format(P.Lambda))

ax02.set_ylabel(r"$N_{t}$")
ax02.set_xlabel(r"$t$")

ax02.legend(loc='upper left', ncol=2, framealpha=0.5)
fig02.savefig('poisson_process.png', format='png', dpi=300, bbox_inches='tight',  pad_inches=0)
# -*- coding: utf-8 -*-
"""Численное решение СДУ для множества траекторий винеровского процесса
в параллельном режиме. Параллелизация осуществляется с помощью стандартного
модуля multiprocessing"""
import multiprocessing as mp
import numpy as np
from typing import Tuple, Callable, Union
import sys

sys.path.append('../')
import stochastic


def calculation(drift_vector: Callable, diffusion_matrix: Callable, simulations_num: int, seed: int,
                x_0: np.ndarray, process_id: int, N: int, time_interval: Tuple[float, float], dim: int,
                test_function: Callable) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """Функция, которая будет численно интегрировать СДУ несколько раз и вычислять статистические
    величины от набора решений

    calculation(drift_vector, diffusion_matrix, simulations_num, seed, x_0, N, time_interval,
                dim, test_function=None) -> (mean_x, min_x, max_x)

    Parameters
    ----------
    drift_vector : function
        Вектор сноса.
    diffusion_matrix : function
        Матрица диффузии.
    simulations_num : int
        Число симуляций.
    seed : int
        Seed для генератора, чтобы каждый процесс генерировал свою последовательность.
    x_0 : 1-d array
        Начальное значение траектории СДУ.
    process_id :
        Номер процесса.
    N : int
        Число интервалов винеровского процесса
    time_interval : tuple of floats
        Временной интервал, по умолчанию (0.0, 1.0)
    dim : int
        Размерность винеровского процесса, по умолчанию 2.
    test_function: function
        Функция для проверки на адекватность, по умолчанию ее нет.

    Returns
    -------
    mean_x, min_x, max_x : arrays of floats
        Среднее, минимум и максимум по всем сгенерированным траекториям
    """
    mean_x = np.empty(shape=N + 1)
    min_x = np.empty(shape=N + 1)
    max_x = np.empty(shape=N + 1)

    # инициализируем генератор
    np.random.seed(seed)
    for simulation in range(simulations_num):
        print("\rid: {0}, step: {1} ".format(process_id, simulation), end="", flush=True)
        # Генерируем новые винеровские процессы до тех пор, пока до конца не доживут все особи
        while True:
            W = stochastic.WienerProcess(N=N, dim=dim, time_interval=time_interval)
            try:
                x_num = stochastic.euler_maruyama_wm(drift_vector, diffusion_matrix, x_0, W,
                                                     test_function=test_function)
            except ValueError as err:
                # print('P{0}'.format(process_id), err, flush=True)
                continue
            else:
                break
        # За первый проход выделяется память
        if simulation == 0:
            mean_x = np.copy(x_num)
            min_x = np.copy(x_num)
            max_x = np.copy(x_num)
        else:
            # Среднее https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Online_algorithm
            mean_x = mean_x + (x_num - mean_x) / float(simulation + 1)
            # Максимальное и минимальное из двух массивов
            max_x = np.maximum(max_x, x_num)
            min_x = np.minimum(min_x, x_num)

    return mean_x, min_x, max_x


def run_parallel(drift_vector: Callable[[float, Union[float, np.ndarray]], Union[float, np.ndarray]],
                 diffusion_matrix: Callable[[float, Union[float, np.ndarray]], Union[float, np.ndarray]],
                 simulations_num: int = 100, process_num: int = 4, **kwargs) -> Tuple:
    """
    run_parallel(drift_vector, diffusion_matrix, simulations_num, process_num)

    Parameters
    ----------
    drift_vector : function
        Вектор сноса.
    diffusion_matrix : function
        Матрица диффузии.
    simulations_num : int
        Число симуляций.
    process_num : int
        Число процессов, каждый из которых проведет simulations_num симуляций

    Other Parameters
    ----------------
    x_0 : 1-d array
        Начальное значение траектории СДУ.
    N : int
        Число интервалов винеровского процесса
    time_interval : tuple of floats
        Временной интервал, по умолчанию (0.0, 1.0)
    dim : int
        Размерность винеровского процесса, по умолчанию 2.
    test_function: function
        Функция для проверки на адекватность, по умолчанию ее нет.

    Returns
    -------
    mean_x, min_x, max_x : arrays of floats
        Среднее, минимум и максимум по всем сгенерированным траекториям
    """
    # Разбираемся с именованными аргументами
    N = kwargs.pop('N', 1000)
    x_0 = kwargs.pop('x_0', (50.1, 10.0))
    time_int = kwargs.pop('time_interval', (0.0, 1.0))
    dim = kwargs.pop('dim', 2)
    test = kwargs.pop('test_function', None)
    print(test)
    seeds = np.random.random_integers(low=0, high=2 ** 29, size=process_num)

    print("Число процессов: {0}\nЧисло симуляций: {1}".format(process_num, simulations_num))

    # Создаем пул процессов и запускаем их
    pool = mp.Pool(processes=process_num)
    results = []
    for id, seed in enumerate(seeds):
        args = (drift_vector, diffusion_matrix, simulations_num, seed, x_0, id, N, time_int, dim, test)
        process = pool.apply_async(func=calculation, args=args)
        results.append(process)

    final_mean = np.sum([r.get()[0] for r in results], axis=0) / float(process_num)
    final_min = np.min([r.get()[1] for r in results], axis=0)
    final_max = np.max([r.get()[2] for r in results], axis=0)

    # Сохраняем результаты
    np.save('mean.npy', final_mean)
    np.save('max.npy', final_max)
    np.save('min.npy', final_min)

    return final_mean, final_min, final_max


if __name__ == '__main__':

    def matrix_sqrt(M):
        """Вычисление квадратного корня из матрицы"""
        U, s, V = np.linalg.svd(M, full_matrices=True)
        S = np.diag(s)
        return np.dot(U, np.dot(np.sqrt(S), V))


    a, k1, k2, k3 = (0.5, 3.0, 0.05, 2.5)


    def f(t, x):
        """Вектор сноса"""
        return np.array([k1 * a * x[0] - k2 * x[0] * x[1], k2 * x[0] * x[1] - k3 * x[1]])


    def G(t, x):
        """Матрица диффузии"""
        return matrix_sqrt(np.array([[x[0] * (k1 * a + k2 * x[1]), -k2 * x[0] * x[1]],
                                     [-k2 * x[0] * x[1], x[1] * (k2 * x[0] + k3)]]))


    def test(x):
        """Проверка решения на физический смысл. Функция передается в метод численного
        решения СДУ. В данном случае вектор x должен быть неотрицательным."""
        # Если хотя бы один компонент x < 0, то
        # следует прервать итерации метода.
        if np.any(x <= 0):
            return True
        else:
            return False

    sim_num = 1000
    proc_num = 4
    kargs = dict(N=1000, x_0=(50.1, 10.0), time_int=(0.0, 20.0), dim=2, test_function=test)
    run_parallel(f, G, simulations_num=sim_num, process_num=proc_num, **kargs)

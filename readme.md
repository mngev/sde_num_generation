# Численные методы для решения систем СДУ (стохастических дифференциальных уравнений)

## Общие сведения

Данный репозиторий содержит два взаимосвязанных модуля для языка `Python`.

1. Модуль `stochastic` реализующий винеровский случайный процесс и численные методы типа Рунге-Кутты для решения как скалярных стохастических дифференциальных уравнений в форме Ито, так и систем СДУ.
2. Набор скриптов `generator` для автоматической генерации кода для стохастических численных методов типа Рунге-Кутты.

## Установка и использование

Для работы модуля `stochastic` необходим `Python 3.5` или новее (в связи с тем, что используются аннотации типов аргументов функций) и библиотека `NumPy`. Скрипты из каталога `generator` кроме всего прочего используют в своей работе шаблонизатор [`Jinja2`](http://jinja.pocoo.org). При использовании `Anaconda Python` обе библиотеки вместе с зависимостями устанавливаются одной командой
```bash
conda install numpy jinja2
```

Для использования модуля `stochastic` достаточно скопировать каталог `stochastic` в директорию с вашими программами и импортировать как стандартный пакет языка `Python`:
```python
import stochastic
```

## Реализованные численные методы

Реализованы стохастические численные методы типа Рунге-Кутты высокого порядка сильной и слабой сходимости, детально разработанные в работах Росслера. Коэффициенты методов взяты в основном из следующих источников.

- Andreas Rößler. Runge-Kutta Methods for the Numerical Solution of Stochastic Differential Equations : Ph.D. thesis / Andreas Rößler ; Technischen Universität Darmstadt. — Darmstadt, 2003.— februar.
- Rößler A. Strong and Weak Approximation Methods for Stochastic Differential Equations – Some Recent Developments / Department Mathematik. Schwerpunkt Mathematische Statistik und Stochastische Prozesse.—2010.
- Debrabant K., Rößler A. Continuous weak approximation for stochastic differential equations // Journal of Computational and Applied Mathematics. — 2008. — no. 214. — P. 259–273.
- Debrabant K., Rößler A. Classification of Stochastic Runge–Kutta Methods for the Weak Approximation of Stochastic Differential Equations / Technische Universität Darmstadt, Fachbereich Mathematik.—2013.— Mar.— arXiv:1303.4510v1.

Коэффициенты методов и другая информация содержится в директории `generators/tables` в формате `JSON`. Подробные численные схемы некоторых методов [см pdf файл](generators/latex/main.pdf)

Все названия функций модуля `stochastic` названы по единой схеме. Функции, реализующие методы с сильной сходимостью снабжены префиксом `strong` в названии, например `strong_srk1w4` — сильный метод метод Рунге-Кутты номер 1 для 4-х мерного винеровского процесса (`w4`). Аббревиатура `srk` едина для всех методов типа Рунге-Кутты. Номер метода условен и берется из `JSON` файлов (см. описание структуры каталогов ниже). Функции слабых численных методов имеют прификс `weak`.

Для примера приведем здесь описание параметров функции, реализующей  стохастический метод Рунге-Кутты с сильной сходимостью `ps=1.0` для векторного винеровского процесса.
```python
strong_srk1w2(f, g, x_0, wiener, test_function=None, keep_only_last=False) -> List[ndarray[float]]
```

#### Параметры функции

- `f` : `function f(t: float, x: ndarray) -> ndarray` -- векторная функция сноса (вектор сноса).
- `g` : `function G(t: float, x: ndarray) -> ndarray` -- матричная функция диффузии (матрица диффузии).
- `x_0` : `array of float` -- начальная точка.
- `wiener` : `WienerProcess` -- ведущий (driving) винеровский процесс в виде объекта, который следует создать до вызова функции.
- `test_function` : function `test(x: ndarray of float) -> bool` -- функция, которая проверят вычисленные значения `x_n` на физический смысл, например >=0 в случае хищника-жертвы. В случае, если она возвращает `False` будет выброшена ошибка `ValueError`.
- `keep_only_last` : `bool` -- сохранять только последнее вычисленное значение или все значения.

#### Функция возвращает следующие значения

- If `keep_only_last = True` return float
- If `keep_only_last = False` return list of ndarray of float

#### Исключения

Выбрасывается исключение типа `ValueError` -- если `test_function` возвращает `False` то это означает потерю физического смысла и вычисления останавливаются.


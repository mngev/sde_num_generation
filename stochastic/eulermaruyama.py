from .stochasticprocess import WienerProcess
from typing import Callable, Iterable, Union, List
import numpy as np


def euler_maruyama(drift: Callable[[float, float], float], diffusion: Callable[[float, float], float],
                   x_0: float, wiener: WienerProcess, test_function: Callable[[float], bool]=None,
                   keep_only_last=False) -> Union[List[float], float]:
    """
    Метод Эйлера-Маруямы для скалярного винеровского процесса

    Parameters
    ----------
    drift : `function f(t, x) -> float`
        Скалярная функция сноса (вектор сноса для скалярного случая).
    diffusion : `function G(t, x) -> float`
        Функция диффузии (матрица диффузии для скалярного случая).
    x_0 : float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops.
    """
    xs = []
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt

    if not keep_only_last:
        xs.append(x)
    else:
        pass

    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
    # Сам алгоритм
    for dw in wiener.dx:
        x = x + drift(t0, x) * h + diffusion(t0, x) * dw
        t0 = t0 + h
        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
        if not keep_only_last:
            xs.append(x)
        else:
            pass

    if keep_only_last:
        return x
    else:
        return xs


def euler_maruyama_wm(drift_vector: Callable[[float, np.ndarray], np.ndarray],
                      diffusion_matrix: Callable[[float, np.ndarray], np.ndarray], x_0: np.ndarray,
                      wiener: WienerProcess, test_function: Callable[[np.ndarray], bool]=None,
                      keep_only_last=False) -> Union[List[np.ndarray],np.ndarray]:
    """Метод Эйлера-Маруямы для векторного винеровского процесса

    Parameters
    ----------
    drift_vector : `function f(t: float, x: ndarray) -> ndarray`
        Векторная функция сноса (вектор сноса).
    diffusion_matrix : `function G(t: float, x: ndarray) -> ndarray`
        Матричная функция диффузии (матрица диффузии).
    x_0 : array of float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: ndarray of float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of ndarray of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops.
    """
    xs = []
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt

    if not keep_only_last:
        xs.append(x)
    else:
        pass

    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
    # Сам алгоритм
    for dw in wiener.dx:
        x = x + drift_vector(t0, x) * h + np.tensordot(diffusion_matrix(t0, x), dw, axes=(1, 0))
        t0 = t0 + h
        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
        if not keep_only_last:
            xs.append(x)
        else:
            pass

    if keep_only_last:
        return x
    else:
        return xs
from .stochasticprocess import WienerProcess
from typing import Callable, List, Union, Iterable
import numpy as np

# Пока функции используют аппроксимированные интегралы Ито, вместо более
# простых распределений, которые можно использовать для слабых методов

    
def weak_srk1w2(f: Callable[[float, np.ndarray], np.ndarray], g: Callable[[float, np.ndarray], np.ndarray], x_0: np.ndarray, wiener: WienerProcess, test_function: Callable[[np.ndarray], bool]=None, keep_only_last: bool=False) -> Union[List[np.ndarray], np.ndarray]:
    """Стохастический метод Рунге-Кутты со слабой сходимостью `ps=2.0`
    weak_srk1w2(f, g, x_0, wiener, last=False) -> List[ndarray[float]]

    Parameters
    ----------
    f : `function f(t: float, x: ndarray) -> ndarray`
        Векторная функция сноса (вектор сноса).
    g : `function G(t: float, x: ndarray) -> ndarray`
        Матричная функция диффузии (матрица диффузии).
    x_0 : array of float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: ndarray of float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of ndarray of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops.
    """
    # инициализация коэффициентов метода
    A0_21 = 1
    A1_21 = 0.25
    B0_21 = 0.33333333333333331
    B1_21 = 0.5
    B2_21 = 1
    A0_31 = 0.1736111111111111
    A0_32 = 0.24305555555555555
    A1_31 = 0.25
    B0_31 = -0.83333333333333337
    B1_31 = -0.5
    B2_31 = -1
    a_1 = 0.10000000000000001
    a_2 = 0.21428571428571427
    a_3 = 0.68571428571428572
    c0_2 = 1
    c0_3 = 0.41666666666666669
    c1_2 = 0.25
    c1_3 = 0.25
    b1_1 = 1
    b1_2 = -1
    b1_3 = -1
    b2_2 = 1
    b2_3 = -1
    b3_1 = 0.5
    b3_2 = -0.25
    b3_3 = -0.25
    b4_2 = 0.5
    b4_3 = -0.5
    # Список для сохранения результатов
    xs = []
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt
    sqrth = np.sqrt(h)
    
    if not keep_only_last:
        xs.append(x)
    else:
        pass

    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
    
    # кратные интегралы Ито для векторного процесса
    Ito2 = wiener.ito_integral_2()

    for I1, I2 in zip(wiener.dx, Ito2):
        X0_1 = x
        X1_1 = x            
        X2_1 = x            
        
        Xhat1_1 = x            
        Xhat2_1 = x            
        X0_2 = x + h * (A0_21 * f(t0, X0_1)) + B0_21 * g(t0, X1_1)[:, 0] * I1[0] + B0_21 * g(t0, X2_1)[:, 1] * I1[1]
        X1_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X1_1)[:, 0] * sqrth            
        X2_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X2_1)[:, 1] * sqrth            
        
        Xhat1_2 = x + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth            
        Xhat2_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth            
        X0_3 = x + h * (A0_31 * f(t0, X0_1) + A0_32 * f(t0 + c0_2 * h, X0_2)) + B0_31 * g(t0, X1_1)[:, 0] * I1[0] + B0_31 * g(t0, X2_1)[:, 1] * I1[1]
        X1_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X1_1)[:, 0] * sqrth            
        X2_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X2_1)[:, 1] * sqrth            
        
        Xhat1_3 = x + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth            
        Xhat2_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth            

        x = x + h * (a_1 * f(t0, X0_1) + a_2 * f(t0 + c0_2 * h, X0_2) + a_3 * f(t0 + c0_3 * h, X0_3)) +(b1_1 * I1[0]) * g(t0, X1_1)[:, 0] + (b1_2 * I1[0]+b2_2 * I2[1, 1] / sqrth) * g(t0 + c1_2 * h, X1_2)[:, 0] + (b1_3 * I1[0]+b2_3 * I2[1, 1] / sqrth) * g(t0 + c1_3 * h, X1_3)[:, 0] + (b1_1 * I1[1]) * g(t0, X2_1)[:, 1] + (b1_2 * I1[1]+b2_2 * I2[2, 2] / sqrth) * g(t0 + c1_2 * h, X2_2)[:, 1] + (b1_3 * I1[1]+b2_3 * I2[2, 2] / sqrth) * g(t0 + c1_3 * h, X2_3)[:, 1]+(b3_1 * I1[0]) * g(t0, Xhat1_1)[:, 0] + (b3_2 * I1[0]+b4_2 * sqrth) * g(t0, Xhat1_2)[:, 0] + (b3_3 * I1[0]+b4_3 * sqrth) * g(t0, Xhat1_3)[:, 0] + (b3_1 * I1[1]) * g(t0, Xhat2_1)[:, 1] + (b3_2 * I1[1]+b4_2 * sqrth) * g(t0, Xhat2_2)[:, 1] + (b3_3 * I1[1]+b4_3 * sqrth) * g(t0, Xhat2_3)[:, 1]        
        t0 = t0 + h

        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
        if not keep_only_last:
            xs.append(x)
        else:
            pass

    if keep_only_last:
        return x
    else:
        return xs


def weak_srk1w3(f: Callable[[float, np.ndarray], np.ndarray], g: Callable[[float, np.ndarray], np.ndarray], x_0: np.ndarray, wiener: WienerProcess, test_function: Callable[[np.ndarray], bool]=None, keep_only_last: bool=False) -> Union[List[np.ndarray], np.ndarray]:
    """Стохастический метод Рунге-Кутты со слабой сходимостью `ps=2.0`
    weak_srk1w3(f, g, x_0, wiener, last=False) -> List[ndarray[float]]

    Parameters
    ----------
    f : `function f(t: float, x: ndarray) -> ndarray`
        Векторная функция сноса (вектор сноса).
    g : `function G(t: float, x: ndarray) -> ndarray`
        Матричная функция диффузии (матрица диффузии).
    x_0 : array of float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: ndarray of float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of ndarray of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops.
    """
    # инициализация коэффициентов метода
    A0_21 = 1
    A1_21 = 0.25
    B0_21 = 0.33333333333333331
    B1_21 = 0.5
    B2_21 = 1
    A0_31 = 0.1736111111111111
    A0_32 = 0.24305555555555555
    A1_31 = 0.25
    B0_31 = -0.83333333333333337
    B1_31 = -0.5
    B2_31 = -1
    a_1 = 0.10000000000000001
    a_2 = 0.21428571428571427
    a_3 = 0.68571428571428572
    c0_2 = 1
    c0_3 = 0.41666666666666669
    c1_2 = 0.25
    c1_3 = 0.25
    b1_1 = 1
    b1_2 = -1
    b1_3 = -1
    b2_2 = 1
    b2_3 = -1
    b3_1 = 0.5
    b3_2 = -0.25
    b3_3 = -0.25
    b4_2 = 0.5
    b4_3 = -0.5
    # Список для сохранения результатов
    xs = []
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt
    sqrth = np.sqrt(h)
    
    if not keep_only_last:
        xs.append(x)
    else:
        pass

    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
    
    # кратные интегралы Ито для векторного процесса
    Ito2 = wiener.ito_integral_2()

    for I1, I2 in zip(wiener.dx, Ito2):
        X0_1 = x
        X1_1 = x            
        X2_1 = x            
        X3_1 = x            
        
        Xhat1_1 = x            
        Xhat2_1 = x            
        Xhat3_1 = x            
        X0_2 = x + h * (A0_21 * f(t0, X0_1)) + B0_21 * g(t0, X1_1)[:, 0] * I1[0] + B0_21 * g(t0, X2_1)[:, 1] * I1[1] + B0_21 * g(t0, X3_1)[:, 2] * I1[2]
        X1_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X1_1)[:, 0] * sqrth            
        X2_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X2_1)[:, 1] * sqrth            
        X3_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X3_1)[:, 2] * sqrth            
        
        Xhat1_2 = x + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth            
        Xhat2_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth            
        Xhat3_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth            
        X0_3 = x + h * (A0_31 * f(t0, X0_1) + A0_32 * f(t0 + c0_2 * h, X0_2)) + B0_31 * g(t0, X1_1)[:, 0] * I1[0] + B0_31 * g(t0, X2_1)[:, 1] * I1[1] + B0_31 * g(t0, X3_1)[:, 2] * I1[2]
        X1_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X1_1)[:, 0] * sqrth            
        X2_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X2_1)[:, 1] * sqrth            
        X3_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X3_1)[:, 2] * sqrth            
        
        Xhat1_3 = x + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth            
        Xhat2_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth            
        Xhat3_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth            

        x = x + h * (a_1 * f(t0, X0_1) + a_2 * f(t0 + c0_2 * h, X0_2) + a_3 * f(t0 + c0_3 * h, X0_3)) +(b1_1 * I1[0]) * g(t0, X1_1)[:, 0] + (b1_2 * I1[0]+b2_2 * I2[1, 1] / sqrth) * g(t0 + c1_2 * h, X1_2)[:, 0] + (b1_3 * I1[0]+b2_3 * I2[1, 1] / sqrth) * g(t0 + c1_3 * h, X1_3)[:, 0] + (b1_1 * I1[1]) * g(t0, X2_1)[:, 1] + (b1_2 * I1[1]+b2_2 * I2[2, 2] / sqrth) * g(t0 + c1_2 * h, X2_2)[:, 1] + (b1_3 * I1[1]+b2_3 * I2[2, 2] / sqrth) * g(t0 + c1_3 * h, X2_3)[:, 1] + (b1_1 * I1[2]) * g(t0, X3_1)[:, 2] + (b1_2 * I1[2]+b2_2 * I2[3, 3] / sqrth) * g(t0 + c1_2 * h, X3_2)[:, 2] + (b1_3 * I1[2]+b2_3 * I2[3, 3] / sqrth) * g(t0 + c1_3 * h, X3_3)[:, 2]+(b3_1 * I1[0]) * g(t0, Xhat1_1)[:, 0] + (b3_2 * I1[0]+b4_2 * sqrth) * g(t0, Xhat1_2)[:, 0] + (b3_3 * I1[0]+b4_3 * sqrth) * g(t0, Xhat1_3)[:, 0] + (b3_1 * I1[1]) * g(t0, Xhat2_1)[:, 1] + (b3_2 * I1[1]+b4_2 * sqrth) * g(t0, Xhat2_2)[:, 1] + (b3_3 * I1[1]+b4_3 * sqrth) * g(t0, Xhat2_3)[:, 1] + (b3_1 * I1[2]) * g(t0, Xhat3_1)[:, 2] + (b3_2 * I1[2]+b4_2 * sqrth) * g(t0, Xhat3_2)[:, 2] + (b3_3 * I1[2]+b4_3 * sqrth) * g(t0, Xhat3_3)[:, 2]        
        t0 = t0 + h

        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
        if not keep_only_last:
            xs.append(x)
        else:
            pass

    if keep_only_last:
        return x
    else:
        return xs


def weak_srk1w4(f: Callable[[float, np.ndarray], np.ndarray], g: Callable[[float, np.ndarray], np.ndarray], x_0: np.ndarray, wiener: WienerProcess, test_function: Callable[[np.ndarray], bool]=None, keep_only_last: bool=False) -> Union[List[np.ndarray], np.ndarray]:
    """Стохастический метод Рунге-Кутты со слабой сходимостью `ps=2.0`
    weak_srk1w4(f, g, x_0, wiener, last=False) -> List[ndarray[float]]

    Parameters
    ----------
    f : `function f(t: float, x: ndarray) -> ndarray`
        Векторная функция сноса (вектор сноса).
    g : `function G(t: float, x: ndarray) -> ndarray`
        Матричная функция диффузии (матрица диффузии).
    x_0 : array of float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: ndarray of float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of ndarray of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops.
    """
    # инициализация коэффициентов метода
    A0_21 = 1
    A1_21 = 0.25
    B0_21 = 0.33333333333333331
    B1_21 = 0.5
    B2_21 = 1
    A0_31 = 0.1736111111111111
    A0_32 = 0.24305555555555555
    A1_31 = 0.25
    B0_31 = -0.83333333333333337
    B1_31 = -0.5
    B2_31 = -1
    a_1 = 0.10000000000000001
    a_2 = 0.21428571428571427
    a_3 = 0.68571428571428572
    c0_2 = 1
    c0_3 = 0.41666666666666669
    c1_2 = 0.25
    c1_3 = 0.25
    b1_1 = 1
    b1_2 = -1
    b1_3 = -1
    b2_2 = 1
    b2_3 = -1
    b3_1 = 0.5
    b3_2 = -0.25
    b3_3 = -0.25
    b4_2 = 0.5
    b4_3 = -0.5
    # Список для сохранения результатов
    xs = []
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt
    sqrth = np.sqrt(h)
    
    if not keep_only_last:
        xs.append(x)
    else:
        pass

    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
    
    # кратные интегралы Ито для векторного процесса
    Ito2 = wiener.ito_integral_2()

    for I1, I2 in zip(wiener.dx, Ito2):
        X0_1 = x
        X1_1 = x            
        X2_1 = x            
        X3_1 = x            
        X4_1 = x            
        
        Xhat1_1 = x            
        Xhat2_1 = x            
        Xhat3_1 = x            
        Xhat4_1 = x            
        X0_2 = x + h * (A0_21 * f(t0, X0_1)) + B0_21 * g(t0, X1_1)[:, 0] * I1[0] + B0_21 * g(t0, X2_1)[:, 1] * I1[1] + B0_21 * g(t0, X3_1)[:, 2] * I1[2] + B0_21 * g(t0, X4_1)[:, 3] * I1[3]
        X1_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X1_1)[:, 0] * sqrth            
        X2_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X2_1)[:, 1] * sqrth            
        X3_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X3_1)[:, 2] * sqrth            
        X4_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X4_1)[:, 3] * sqrth            
        
        Xhat1_2 = x + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 1] / sqrth            
        Xhat2_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 2] / sqrth            
        Xhat3_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 3] / sqrth            
        Xhat4_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 4] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 4] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 4] / sqrth            
        X0_3 = x + h * (A0_31 * f(t0, X0_1) + A0_32 * f(t0 + c0_2 * h, X0_2)) + B0_31 * g(t0, X1_1)[:, 0] * I1[0] + B0_31 * g(t0, X2_1)[:, 1] * I1[1] + B0_31 * g(t0, X3_1)[:, 2] * I1[2] + B0_31 * g(t0, X4_1)[:, 3] * I1[3]
        X1_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X1_1)[:, 0] * sqrth            
        X2_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X2_1)[:, 1] * sqrth            
        X3_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X3_1)[:, 2] * sqrth            
        X4_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X4_1)[:, 3] * sqrth            
        
        Xhat1_3 = x + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 1] / sqrth            
        Xhat2_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 2] / sqrth            
        Xhat3_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 3] / sqrth            
        Xhat4_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 4] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 4] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 4] / sqrth            

        x = x + h * (a_1 * f(t0, X0_1) + a_2 * f(t0 + c0_2 * h, X0_2) + a_3 * f(t0 + c0_3 * h, X0_3)) +(b1_1 * I1[0]) * g(t0, X1_1)[:, 0] + (b1_2 * I1[0]+b2_2 * I2[1, 1] / sqrth) * g(t0 + c1_2 * h, X1_2)[:, 0] + (b1_3 * I1[0]+b2_3 * I2[1, 1] / sqrth) * g(t0 + c1_3 * h, X1_3)[:, 0] + (b1_1 * I1[1]) * g(t0, X2_1)[:, 1] + (b1_2 * I1[1]+b2_2 * I2[2, 2] / sqrth) * g(t0 + c1_2 * h, X2_2)[:, 1] + (b1_3 * I1[1]+b2_3 * I2[2, 2] / sqrth) * g(t0 + c1_3 * h, X2_3)[:, 1] + (b1_1 * I1[2]) * g(t0, X3_1)[:, 2] + (b1_2 * I1[2]+b2_2 * I2[3, 3] / sqrth) * g(t0 + c1_2 * h, X3_2)[:, 2] + (b1_3 * I1[2]+b2_3 * I2[3, 3] / sqrth) * g(t0 + c1_3 * h, X3_3)[:, 2] + (b1_1 * I1[3]) * g(t0, X4_1)[:, 3] + (b1_2 * I1[3]+b2_2 * I2[4, 4] / sqrth) * g(t0 + c1_2 * h, X4_2)[:, 3] + (b1_3 * I1[3]+b2_3 * I2[4, 4] / sqrth) * g(t0 + c1_3 * h, X4_3)[:, 3]+(b3_1 * I1[0]) * g(t0, Xhat1_1)[:, 0] + (b3_2 * I1[0]+b4_2 * sqrth) * g(t0, Xhat1_2)[:, 0] + (b3_3 * I1[0]+b4_3 * sqrth) * g(t0, Xhat1_3)[:, 0] + (b3_1 * I1[1]) * g(t0, Xhat2_1)[:, 1] + (b3_2 * I1[1]+b4_2 * sqrth) * g(t0, Xhat2_2)[:, 1] + (b3_3 * I1[1]+b4_3 * sqrth) * g(t0, Xhat2_3)[:, 1] + (b3_1 * I1[2]) * g(t0, Xhat3_1)[:, 2] + (b3_2 * I1[2]+b4_2 * sqrth) * g(t0, Xhat3_2)[:, 2] + (b3_3 * I1[2]+b4_3 * sqrth) * g(t0, Xhat3_3)[:, 2] + (b3_1 * I1[3]) * g(t0, Xhat4_1)[:, 3] + (b3_2 * I1[3]+b4_2 * sqrth) * g(t0, Xhat4_2)[:, 3] + (b3_3 * I1[3]+b4_3 * sqrth) * g(t0, Xhat4_3)[:, 3]        
        t0 = t0 + h

        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
        if not keep_only_last:
            xs.append(x)
        else:
            pass

    if keep_only_last:
        return x
    else:
        return xs


def weak_srk1w5(f: Callable[[float, np.ndarray], np.ndarray], g: Callable[[float, np.ndarray], np.ndarray], x_0: np.ndarray, wiener: WienerProcess, test_function: Callable[[np.ndarray], bool]=None, keep_only_last: bool=False) -> Union[List[np.ndarray], np.ndarray]:
    """Стохастический метод Рунге-Кутты со слабой сходимостью `ps=2.0`
    weak_srk1w5(f, g, x_0, wiener, last=False) -> List[ndarray[float]]

    Parameters
    ----------
    f : `function f(t: float, x: ndarray) -> ndarray`
        Векторная функция сноса (вектор сноса).
    g : `function G(t: float, x: ndarray) -> ndarray`
        Матричная функция диффузии (матрица диффузии).
    x_0 : array of float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: ndarray of float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of ndarray of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops.
    """
    # инициализация коэффициентов метода
    A0_21 = 1
    A1_21 = 0.25
    B0_21 = 0.33333333333333331
    B1_21 = 0.5
    B2_21 = 1
    A0_31 = 0.1736111111111111
    A0_32 = 0.24305555555555555
    A1_31 = 0.25
    B0_31 = -0.83333333333333337
    B1_31 = -0.5
    B2_31 = -1
    a_1 = 0.10000000000000001
    a_2 = 0.21428571428571427
    a_3 = 0.68571428571428572
    c0_2 = 1
    c0_3 = 0.41666666666666669
    c1_2 = 0.25
    c1_3 = 0.25
    b1_1 = 1
    b1_2 = -1
    b1_3 = -1
    b2_2 = 1
    b2_3 = -1
    b3_1 = 0.5
    b3_2 = -0.25
    b3_3 = -0.25
    b4_2 = 0.5
    b4_3 = -0.5
    # Список для сохранения результатов
    xs = []
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt
    sqrth = np.sqrt(h)
    
    if not keep_only_last:
        xs.append(x)
    else:
        pass

    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
    
    # кратные интегралы Ито для векторного процесса
    Ito2 = wiener.ito_integral_2()

    for I1, I2 in zip(wiener.dx, Ito2):
        X0_1 = x
        X1_1 = x            
        X2_1 = x            
        X3_1 = x            
        X4_1 = x            
        X5_1 = x            
        
        Xhat1_1 = x            
        Xhat2_1 = x            
        Xhat3_1 = x            
        Xhat4_1 = x            
        Xhat5_1 = x            
        X0_2 = x + h * (A0_21 * f(t0, X0_1)) + B0_21 * g(t0, X1_1)[:, 0] * I1[0] + B0_21 * g(t0, X2_1)[:, 1] * I1[1] + B0_21 * g(t0, X3_1)[:, 2] * I1[2] + B0_21 * g(t0, X4_1)[:, 3] * I1[3] + B0_21 * g(t0, X5_1)[:, 4] * I1[4]
        X1_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X1_1)[:, 0] * sqrth            
        X2_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X2_1)[:, 1] * sqrth            
        X3_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X3_1)[:, 2] * sqrth            
        X4_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X4_1)[:, 3] * sqrth            
        X5_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X5_1)[:, 4] * sqrth            
        
        Xhat1_2 = x + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 1] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 1] / sqrth            
        Xhat2_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 2] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 2] / sqrth            
        Xhat3_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 3] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 3] / sqrth            
        Xhat4_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 4] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 4] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 4] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 4] / sqrth            
        Xhat5_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 5] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 5] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 5] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 5] / sqrth            
        X0_3 = x + h * (A0_31 * f(t0, X0_1) + A0_32 * f(t0 + c0_2 * h, X0_2)) + B0_31 * g(t0, X1_1)[:, 0] * I1[0] + B0_31 * g(t0, X2_1)[:, 1] * I1[1] + B0_31 * g(t0, X3_1)[:, 2] * I1[2] + B0_31 * g(t0, X4_1)[:, 3] * I1[3] + B0_31 * g(t0, X5_1)[:, 4] * I1[4]
        X1_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X1_1)[:, 0] * sqrth            
        X2_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X2_1)[:, 1] * sqrth            
        X3_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X3_1)[:, 2] * sqrth            
        X4_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X4_1)[:, 3] * sqrth            
        X5_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X5_1)[:, 4] * sqrth            
        
        Xhat1_3 = x + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 1] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 1] / sqrth            
        Xhat2_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 2] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 2] / sqrth            
        Xhat3_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 3] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 3] / sqrth            
        Xhat4_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 4] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 4] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 4] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 4] / sqrth            
        Xhat5_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 5] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 5] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 5] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 5] / sqrth            

        x = x + h * (a_1 * f(t0, X0_1) + a_2 * f(t0 + c0_2 * h, X0_2) + a_3 * f(t0 + c0_3 * h, X0_3)) +(b1_1 * I1[0]) * g(t0, X1_1)[:, 0] + (b1_2 * I1[0]+b2_2 * I2[1, 1] / sqrth) * g(t0 + c1_2 * h, X1_2)[:, 0] + (b1_3 * I1[0]+b2_3 * I2[1, 1] / sqrth) * g(t0 + c1_3 * h, X1_3)[:, 0] + (b1_1 * I1[1]) * g(t0, X2_1)[:, 1] + (b1_2 * I1[1]+b2_2 * I2[2, 2] / sqrth) * g(t0 + c1_2 * h, X2_2)[:, 1] + (b1_3 * I1[1]+b2_3 * I2[2, 2] / sqrth) * g(t0 + c1_3 * h, X2_3)[:, 1] + (b1_1 * I1[2]) * g(t0, X3_1)[:, 2] + (b1_2 * I1[2]+b2_2 * I2[3, 3] / sqrth) * g(t0 + c1_2 * h, X3_2)[:, 2] + (b1_3 * I1[2]+b2_3 * I2[3, 3] / sqrth) * g(t0 + c1_3 * h, X3_3)[:, 2] + (b1_1 * I1[3]) * g(t0, X4_1)[:, 3] + (b1_2 * I1[3]+b2_2 * I2[4, 4] / sqrth) * g(t0 + c1_2 * h, X4_2)[:, 3] + (b1_3 * I1[3]+b2_3 * I2[4, 4] / sqrth) * g(t0 + c1_3 * h, X4_3)[:, 3] + (b1_1 * I1[4]) * g(t0, X5_1)[:, 4] + (b1_2 * I1[4]+b2_2 * I2[5, 5] / sqrth) * g(t0 + c1_2 * h, X5_2)[:, 4] + (b1_3 * I1[4]+b2_3 * I2[5, 5] / sqrth) * g(t0 + c1_3 * h, X5_3)[:, 4]+(b3_1 * I1[0]) * g(t0, Xhat1_1)[:, 0] + (b3_2 * I1[0]+b4_2 * sqrth) * g(t0, Xhat1_2)[:, 0] + (b3_3 * I1[0]+b4_3 * sqrth) * g(t0, Xhat1_3)[:, 0] + (b3_1 * I1[1]) * g(t0, Xhat2_1)[:, 1] + (b3_2 * I1[1]+b4_2 * sqrth) * g(t0, Xhat2_2)[:, 1] + (b3_3 * I1[1]+b4_3 * sqrth) * g(t0, Xhat2_3)[:, 1] + (b3_1 * I1[2]) * g(t0, Xhat3_1)[:, 2] + (b3_2 * I1[2]+b4_2 * sqrth) * g(t0, Xhat3_2)[:, 2] + (b3_3 * I1[2]+b4_3 * sqrth) * g(t0, Xhat3_3)[:, 2] + (b3_1 * I1[3]) * g(t0, Xhat4_1)[:, 3] + (b3_2 * I1[3]+b4_2 * sqrth) * g(t0, Xhat4_2)[:, 3] + (b3_3 * I1[3]+b4_3 * sqrth) * g(t0, Xhat4_3)[:, 3] + (b3_1 * I1[4]) * g(t0, Xhat5_1)[:, 4] + (b3_2 * I1[4]+b4_2 * sqrth) * g(t0, Xhat5_2)[:, 4] + (b3_3 * I1[4]+b4_3 * sqrth) * g(t0, Xhat5_3)[:, 4]        
        t0 = t0 + h

        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
        if not keep_only_last:
            xs.append(x)
        else:
            pass

    if keep_only_last:
        return x
    else:
        return xs


def weak_srk1w6(f: Callable[[float, np.ndarray], np.ndarray], g: Callable[[float, np.ndarray], np.ndarray], x_0: np.ndarray, wiener: WienerProcess, test_function: Callable[[np.ndarray], bool]=None, keep_only_last: bool=False) -> Union[List[np.ndarray], np.ndarray]:
    """Стохастический метод Рунге-Кутты со слабой сходимостью `ps=2.0`
    weak_srk1w6(f, g, x_0, wiener, last=False) -> List[ndarray[float]]

    Parameters
    ----------
    f : `function f(t: float, x: ndarray) -> ndarray`
        Векторная функция сноса (вектор сноса).
    g : `function G(t: float, x: ndarray) -> ndarray`
        Матричная функция диффузии (матрица диффузии).
    x_0 : array of float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: ndarray of float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of ndarray of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops.
    """
    # инициализация коэффициентов метода
    A0_21 = 1
    A1_21 = 0.25
    B0_21 = 0.33333333333333331
    B1_21 = 0.5
    B2_21 = 1
    A0_31 = 0.1736111111111111
    A0_32 = 0.24305555555555555
    A1_31 = 0.25
    B0_31 = -0.83333333333333337
    B1_31 = -0.5
    B2_31 = -1
    a_1 = 0.10000000000000001
    a_2 = 0.21428571428571427
    a_3 = 0.68571428571428572
    c0_2 = 1
    c0_3 = 0.41666666666666669
    c1_2 = 0.25
    c1_3 = 0.25
    b1_1 = 1
    b1_2 = -1
    b1_3 = -1
    b2_2 = 1
    b2_3 = -1
    b3_1 = 0.5
    b3_2 = -0.25
    b3_3 = -0.25
    b4_2 = 0.5
    b4_3 = -0.5
    # Список для сохранения результатов
    xs = []
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt
    sqrth = np.sqrt(h)
    
    if not keep_only_last:
        xs.append(x)
    else:
        pass

    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
    
    # кратные интегралы Ито для векторного процесса
    Ito2 = wiener.ito_integral_2()

    for I1, I2 in zip(wiener.dx, Ito2):
        X0_1 = x
        X1_1 = x            
        X2_1 = x            
        X3_1 = x            
        X4_1 = x            
        X5_1 = x            
        X6_1 = x            
        
        Xhat1_1 = x            
        Xhat2_1 = x            
        Xhat3_1 = x            
        Xhat4_1 = x            
        Xhat5_1 = x            
        Xhat6_1 = x            
        X0_2 = x + h * (A0_21 * f(t0, X0_1)) + B0_21 * g(t0, X1_1)[:, 0] * I1[0] + B0_21 * g(t0, X2_1)[:, 1] * I1[1] + B0_21 * g(t0, X3_1)[:, 2] * I1[2] + B0_21 * g(t0, X4_1)[:, 3] * I1[3] + B0_21 * g(t0, X5_1)[:, 4] * I1[4] + B0_21 * g(t0, X6_1)[:, 5] * I1[5]
        X1_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X1_1)[:, 0] * sqrth            
        X2_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X2_1)[:, 1] * sqrth            
        X3_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X3_1)[:, 2] * sqrth            
        X4_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X4_1)[:, 3] * sqrth            
        X5_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X5_1)[:, 4] * sqrth            
        X6_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X6_1)[:, 5] * sqrth            
        
        Xhat1_2 = x + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 1] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 1] / sqrth + B2_21 * g(t0, X6_1)[:, 5] * I2[6, 1] / sqrth            
        Xhat2_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 2] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 2] / sqrth + B2_21 * g(t0, X6_1)[:, 5] * I2[6, 2] / sqrth            
        Xhat3_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 3] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 3] / sqrth + B2_21 * g(t0, X6_1)[:, 5] * I2[6, 3] / sqrth            
        Xhat4_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 4] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 4] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 4] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 4] / sqrth + B2_21 * g(t0, X6_1)[:, 5] * I2[6, 4] / sqrth            
        Xhat5_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 5] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 5] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 5] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 5] / sqrth + B2_21 * g(t0, X6_1)[:, 5] * I2[6, 5] / sqrth            
        Xhat6_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 6] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 6] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 6] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 6] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 6] / sqrth            
        X0_3 = x + h * (A0_31 * f(t0, X0_1) + A0_32 * f(t0 + c0_2 * h, X0_2)) + B0_31 * g(t0, X1_1)[:, 0] * I1[0] + B0_31 * g(t0, X2_1)[:, 1] * I1[1] + B0_31 * g(t0, X3_1)[:, 2] * I1[2] + B0_31 * g(t0, X4_1)[:, 3] * I1[3] + B0_31 * g(t0, X5_1)[:, 4] * I1[4] + B0_31 * g(t0, X6_1)[:, 5] * I1[5]
        X1_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X1_1)[:, 0] * sqrth            
        X2_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X2_1)[:, 1] * sqrth            
        X3_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X3_1)[:, 2] * sqrth            
        X4_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X4_1)[:, 3] * sqrth            
        X5_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X5_1)[:, 4] * sqrth            
        X6_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X6_1)[:, 5] * sqrth            
        
        Xhat1_3 = x + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 1] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 1] / sqrth + B2_31 * g(t0, X6_1)[:, 5] * I2[6, 1] / sqrth            
        Xhat2_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 2] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 2] / sqrth + B2_31 * g(t0, X6_1)[:, 5] * I2[6, 2] / sqrth            
        Xhat3_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 3] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 3] / sqrth + B2_31 * g(t0, X6_1)[:, 5] * I2[6, 3] / sqrth            
        Xhat4_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 4] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 4] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 4] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 4] / sqrth + B2_31 * g(t0, X6_1)[:, 5] * I2[6, 4] / sqrth            
        Xhat5_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 5] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 5] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 5] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 5] / sqrth + B2_31 * g(t0, X6_1)[:, 5] * I2[6, 5] / sqrth            
        Xhat6_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 6] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 6] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 6] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 6] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 6] / sqrth            

        x = x + h * (a_1 * f(t0, X0_1) + a_2 * f(t0 + c0_2 * h, X0_2) + a_3 * f(t0 + c0_3 * h, X0_3)) +(b1_1 * I1[0]) * g(t0, X1_1)[:, 0] + (b1_2 * I1[0]+b2_2 * I2[1, 1] / sqrth) * g(t0 + c1_2 * h, X1_2)[:, 0] + (b1_3 * I1[0]+b2_3 * I2[1, 1] / sqrth) * g(t0 + c1_3 * h, X1_3)[:, 0] + (b1_1 * I1[1]) * g(t0, X2_1)[:, 1] + (b1_2 * I1[1]+b2_2 * I2[2, 2] / sqrth) * g(t0 + c1_2 * h, X2_2)[:, 1] + (b1_3 * I1[1]+b2_3 * I2[2, 2] / sqrth) * g(t0 + c1_3 * h, X2_3)[:, 1] + (b1_1 * I1[2]) * g(t0, X3_1)[:, 2] + (b1_2 * I1[2]+b2_2 * I2[3, 3] / sqrth) * g(t0 + c1_2 * h, X3_2)[:, 2] + (b1_3 * I1[2]+b2_3 * I2[3, 3] / sqrth) * g(t0 + c1_3 * h, X3_3)[:, 2] + (b1_1 * I1[3]) * g(t0, X4_1)[:, 3] + (b1_2 * I1[3]+b2_2 * I2[4, 4] / sqrth) * g(t0 + c1_2 * h, X4_2)[:, 3] + (b1_3 * I1[3]+b2_3 * I2[4, 4] / sqrth) * g(t0 + c1_3 * h, X4_3)[:, 3] + (b1_1 * I1[4]) * g(t0, X5_1)[:, 4] + (b1_2 * I1[4]+b2_2 * I2[5, 5] / sqrth) * g(t0 + c1_2 * h, X5_2)[:, 4] + (b1_3 * I1[4]+b2_3 * I2[5, 5] / sqrth) * g(t0 + c1_3 * h, X5_3)[:, 4] + (b1_1 * I1[5]) * g(t0, X6_1)[:, 5] + (b1_2 * I1[5]+b2_2 * I2[6, 6] / sqrth) * g(t0 + c1_2 * h, X6_2)[:, 5] + (b1_3 * I1[5]+b2_3 * I2[6, 6] / sqrth) * g(t0 + c1_3 * h, X6_3)[:, 5]+(b3_1 * I1[0]) * g(t0, Xhat1_1)[:, 0] + (b3_2 * I1[0]+b4_2 * sqrth) * g(t0, Xhat1_2)[:, 0] + (b3_3 * I1[0]+b4_3 * sqrth) * g(t0, Xhat1_3)[:, 0] + (b3_1 * I1[1]) * g(t0, Xhat2_1)[:, 1] + (b3_2 * I1[1]+b4_2 * sqrth) * g(t0, Xhat2_2)[:, 1] + (b3_3 * I1[1]+b4_3 * sqrth) * g(t0, Xhat2_3)[:, 1] + (b3_1 * I1[2]) * g(t0, Xhat3_1)[:, 2] + (b3_2 * I1[2]+b4_2 * sqrth) * g(t0, Xhat3_2)[:, 2] + (b3_3 * I1[2]+b4_3 * sqrth) * g(t0, Xhat3_3)[:, 2] + (b3_1 * I1[3]) * g(t0, Xhat4_1)[:, 3] + (b3_2 * I1[3]+b4_2 * sqrth) * g(t0, Xhat4_2)[:, 3] + (b3_3 * I1[3]+b4_3 * sqrth) * g(t0, Xhat4_3)[:, 3] + (b3_1 * I1[4]) * g(t0, Xhat5_1)[:, 4] + (b3_2 * I1[4]+b4_2 * sqrth) * g(t0, Xhat5_2)[:, 4] + (b3_3 * I1[4]+b4_3 * sqrth) * g(t0, Xhat5_3)[:, 4] + (b3_1 * I1[5]) * g(t0, Xhat6_1)[:, 5] + (b3_2 * I1[5]+b4_2 * sqrth) * g(t0, Xhat6_2)[:, 5] + (b3_3 * I1[5]+b4_3 * sqrth) * g(t0, Xhat6_3)[:, 5]        
        t0 = t0 + h

        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
        if not keep_only_last:
            xs.append(x)
        else:
            pass

    if keep_only_last:
        return x
    else:
        return xs


def weak_srk2w2(f: Callable[[float, np.ndarray], np.ndarray], g: Callable[[float, np.ndarray], np.ndarray], x_0: np.ndarray, wiener: WienerProcess, test_function: Callable[[np.ndarray], bool]=None, keep_only_last: bool=False) -> Union[List[np.ndarray], np.ndarray]:
    """Стохастический метод Рунге-Кутты со слабой сходимостью `ps=2.0`
    weak_srk2w2(f, g, x_0, wiener, last=False) -> List[ndarray[float]]

    Parameters
    ----------
    f : `function f(t: float, x: ndarray) -> ndarray`
        Векторная функция сноса (вектор сноса).
    g : `function G(t: float, x: ndarray) -> ndarray`
        Матричная функция диффузии (матрица диффузии).
    x_0 : array of float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: ndarray of float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of ndarray of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops.
    """
    # инициализация коэффициентов метода
    A0_21 = 1
    A1_21 = 1
    B0_21 = 1
    B1_21 = 1
    B2_21 = 1
    A1_31 = 1
    B1_31 = -1
    B2_31 = -1
    a_1 = 0.5
    a_2 = 0.5
    c0_2 = 1
    c1_2 = 1
    c1_3 = 1
    b1_1 = 0.5
    b1_2 = 0.25
    b1_3 = 0.25
    b2_2 = 0.5
    b2_3 = -0.5
    b3_1 = -0.5
    b3_2 = 0.25
    b3_3 = 0.25
    b4_2 = 0.5
    b4_3 = -0.5
    # Список для сохранения результатов
    xs = []
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt
    sqrth = np.sqrt(h)
    
    if not keep_only_last:
        xs.append(x)
    else:
        pass

    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
    
    # кратные интегралы Ито для векторного процесса
    Ito2 = wiener.ito_integral_2()

    for I1, I2 in zip(wiener.dx, Ito2):
        X0_1 = x
        X1_1 = x            
        X2_1 = x            
        
        Xhat1_1 = x            
        Xhat2_1 = x            
        X0_2 = x + h * (A0_21 * f(t0, X0_1)) + B0_21 * g(t0, X1_1)[:, 0] * I1[0] + B0_21 * g(t0, X2_1)[:, 1] * I1[1]
        X1_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X1_1)[:, 0] * sqrth            
        X2_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X2_1)[:, 1] * sqrth            
        
        Xhat1_2 = x + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth            
        Xhat2_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth            
        X0_3 = x
        X1_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X1_1)[:, 0] * sqrth            
        X2_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X2_1)[:, 1] * sqrth            
        
        Xhat1_3 = x + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth            
        Xhat2_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth            

        x = x + h * (a_1 * f(t0, X0_1) + a_2 * f(t0 + c0_2 * h, X0_2)) +(b1_1 * I1[0]) * g(t0, X1_1)[:, 0] + (b1_2 * I1[0]+b2_2 * I2[1, 1] / sqrth) * g(t0 + c1_2 * h, X1_2)[:, 0] + (b1_3 * I1[0]+b2_3 * I2[1, 1] / sqrth) * g(t0 + c1_3 * h, X1_3)[:, 0] + (b1_1 * I1[1]) * g(t0, X2_1)[:, 1] + (b1_2 * I1[1]+b2_2 * I2[2, 2] / sqrth) * g(t0 + c1_2 * h, X2_2)[:, 1] + (b1_3 * I1[1]+b2_3 * I2[2, 2] / sqrth) * g(t0 + c1_3 * h, X2_3)[:, 1]+(b3_1 * I1[0]) * g(t0, Xhat1_1)[:, 0] + (b3_2 * I1[0]+b4_2 * sqrth) * g(t0, Xhat1_2)[:, 0] + (b3_3 * I1[0]+b4_3 * sqrth) * g(t0, Xhat1_3)[:, 0] + (b3_1 * I1[1]) * g(t0, Xhat2_1)[:, 1] + (b3_2 * I1[1]+b4_2 * sqrth) * g(t0, Xhat2_2)[:, 1] + (b3_3 * I1[1]+b4_3 * sqrth) * g(t0, Xhat2_3)[:, 1]        
        t0 = t0 + h

        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
        if not keep_only_last:
            xs.append(x)
        else:
            pass

    if keep_only_last:
        return x
    else:
        return xs


def weak_srk2w3(f: Callable[[float, np.ndarray], np.ndarray], g: Callable[[float, np.ndarray], np.ndarray], x_0: np.ndarray, wiener: WienerProcess, test_function: Callable[[np.ndarray], bool]=None, keep_only_last: bool=False) -> Union[List[np.ndarray], np.ndarray]:
    """Стохастический метод Рунге-Кутты со слабой сходимостью `ps=2.0`
    weak_srk2w3(f, g, x_0, wiener, last=False) -> List[ndarray[float]]

    Parameters
    ----------
    f : `function f(t: float, x: ndarray) -> ndarray`
        Векторная функция сноса (вектор сноса).
    g : `function G(t: float, x: ndarray) -> ndarray`
        Матричная функция диффузии (матрица диффузии).
    x_0 : array of float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: ndarray of float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of ndarray of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops.
    """
    # инициализация коэффициентов метода
    A0_21 = 1
    A1_21 = 1
    B0_21 = 1
    B1_21 = 1
    B2_21 = 1
    A1_31 = 1
    B1_31 = -1
    B2_31 = -1
    a_1 = 0.5
    a_2 = 0.5
    c0_2 = 1
    c1_2 = 1
    c1_3 = 1
    b1_1 = 0.5
    b1_2 = 0.25
    b1_3 = 0.25
    b2_2 = 0.5
    b2_3 = -0.5
    b3_1 = -0.5
    b3_2 = 0.25
    b3_3 = 0.25
    b4_2 = 0.5
    b4_3 = -0.5
    # Список для сохранения результатов
    xs = []
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt
    sqrth = np.sqrt(h)
    
    if not keep_only_last:
        xs.append(x)
    else:
        pass

    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
    
    # кратные интегралы Ито для векторного процесса
    Ito2 = wiener.ito_integral_2()

    for I1, I2 in zip(wiener.dx, Ito2):
        X0_1 = x
        X1_1 = x            
        X2_1 = x            
        X3_1 = x            
        
        Xhat1_1 = x            
        Xhat2_1 = x            
        Xhat3_1 = x            
        X0_2 = x + h * (A0_21 * f(t0, X0_1)) + B0_21 * g(t0, X1_1)[:, 0] * I1[0] + B0_21 * g(t0, X2_1)[:, 1] * I1[1] + B0_21 * g(t0, X3_1)[:, 2] * I1[2]
        X1_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X1_1)[:, 0] * sqrth            
        X2_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X2_1)[:, 1] * sqrth            
        X3_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X3_1)[:, 2] * sqrth            
        
        Xhat1_2 = x + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth            
        Xhat2_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth            
        Xhat3_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth            
        X0_3 = x
        X1_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X1_1)[:, 0] * sqrth            
        X2_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X2_1)[:, 1] * sqrth            
        X3_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X3_1)[:, 2] * sqrth            
        
        Xhat1_3 = x + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth            
        Xhat2_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth            
        Xhat3_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth            

        x = x + h * (a_1 * f(t0, X0_1) + a_2 * f(t0 + c0_2 * h, X0_2)) +(b1_1 * I1[0]) * g(t0, X1_1)[:, 0] + (b1_2 * I1[0]+b2_2 * I2[1, 1] / sqrth) * g(t0 + c1_2 * h, X1_2)[:, 0] + (b1_3 * I1[0]+b2_3 * I2[1, 1] / sqrth) * g(t0 + c1_3 * h, X1_3)[:, 0] + (b1_1 * I1[1]) * g(t0, X2_1)[:, 1] + (b1_2 * I1[1]+b2_2 * I2[2, 2] / sqrth) * g(t0 + c1_2 * h, X2_2)[:, 1] + (b1_3 * I1[1]+b2_3 * I2[2, 2] / sqrth) * g(t0 + c1_3 * h, X2_3)[:, 1] + (b1_1 * I1[2]) * g(t0, X3_1)[:, 2] + (b1_2 * I1[2]+b2_2 * I2[3, 3] / sqrth) * g(t0 + c1_2 * h, X3_2)[:, 2] + (b1_3 * I1[2]+b2_3 * I2[3, 3] / sqrth) * g(t0 + c1_3 * h, X3_3)[:, 2]+(b3_1 * I1[0]) * g(t0, Xhat1_1)[:, 0] + (b3_2 * I1[0]+b4_2 * sqrth) * g(t0, Xhat1_2)[:, 0] + (b3_3 * I1[0]+b4_3 * sqrth) * g(t0, Xhat1_3)[:, 0] + (b3_1 * I1[1]) * g(t0, Xhat2_1)[:, 1] + (b3_2 * I1[1]+b4_2 * sqrth) * g(t0, Xhat2_2)[:, 1] + (b3_3 * I1[1]+b4_3 * sqrth) * g(t0, Xhat2_3)[:, 1] + (b3_1 * I1[2]) * g(t0, Xhat3_1)[:, 2] + (b3_2 * I1[2]+b4_2 * sqrth) * g(t0, Xhat3_2)[:, 2] + (b3_3 * I1[2]+b4_3 * sqrth) * g(t0, Xhat3_3)[:, 2]        
        t0 = t0 + h

        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
        if not keep_only_last:
            xs.append(x)
        else:
            pass

    if keep_only_last:
        return x
    else:
        return xs


def weak_srk2w4(f: Callable[[float, np.ndarray], np.ndarray], g: Callable[[float, np.ndarray], np.ndarray], x_0: np.ndarray, wiener: WienerProcess, test_function: Callable[[np.ndarray], bool]=None, keep_only_last: bool=False) -> Union[List[np.ndarray], np.ndarray]:
    """Стохастический метод Рунге-Кутты со слабой сходимостью `ps=2.0`
    weak_srk2w4(f, g, x_0, wiener, last=False) -> List[ndarray[float]]

    Parameters
    ----------
    f : `function f(t: float, x: ndarray) -> ndarray`
        Векторная функция сноса (вектор сноса).
    g : `function G(t: float, x: ndarray) -> ndarray`
        Матричная функция диффузии (матрица диффузии).
    x_0 : array of float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: ndarray of float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of ndarray of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops.
    """
    # инициализация коэффициентов метода
    A0_21 = 1
    A1_21 = 1
    B0_21 = 1
    B1_21 = 1
    B2_21 = 1
    A1_31 = 1
    B1_31 = -1
    B2_31 = -1
    a_1 = 0.5
    a_2 = 0.5
    c0_2 = 1
    c1_2 = 1
    c1_3 = 1
    b1_1 = 0.5
    b1_2 = 0.25
    b1_3 = 0.25
    b2_2 = 0.5
    b2_3 = -0.5
    b3_1 = -0.5
    b3_2 = 0.25
    b3_3 = 0.25
    b4_2 = 0.5
    b4_3 = -0.5
    # Список для сохранения результатов
    xs = []
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt
    sqrth = np.sqrt(h)
    
    if not keep_only_last:
        xs.append(x)
    else:
        pass

    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
    
    # кратные интегралы Ито для векторного процесса
    Ito2 = wiener.ito_integral_2()

    for I1, I2 in zip(wiener.dx, Ito2):
        X0_1 = x
        X1_1 = x            
        X2_1 = x            
        X3_1 = x            
        X4_1 = x            
        
        Xhat1_1 = x            
        Xhat2_1 = x            
        Xhat3_1 = x            
        Xhat4_1 = x            
        X0_2 = x + h * (A0_21 * f(t0, X0_1)) + B0_21 * g(t0, X1_1)[:, 0] * I1[0] + B0_21 * g(t0, X2_1)[:, 1] * I1[1] + B0_21 * g(t0, X3_1)[:, 2] * I1[2] + B0_21 * g(t0, X4_1)[:, 3] * I1[3]
        X1_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X1_1)[:, 0] * sqrth            
        X2_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X2_1)[:, 1] * sqrth            
        X3_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X3_1)[:, 2] * sqrth            
        X4_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X4_1)[:, 3] * sqrth            
        
        Xhat1_2 = x + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 1] / sqrth            
        Xhat2_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 2] / sqrth            
        Xhat3_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 3] / sqrth            
        Xhat4_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 4] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 4] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 4] / sqrth            
        X0_3 = x
        X1_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X1_1)[:, 0] * sqrth            
        X2_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X2_1)[:, 1] * sqrth            
        X3_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X3_1)[:, 2] * sqrth            
        X4_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X4_1)[:, 3] * sqrth            
        
        Xhat1_3 = x + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 1] / sqrth            
        Xhat2_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 2] / sqrth            
        Xhat3_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 3] / sqrth            
        Xhat4_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 4] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 4] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 4] / sqrth            

        x = x + h * (a_1 * f(t0, X0_1) + a_2 * f(t0 + c0_2 * h, X0_2)) +(b1_1 * I1[0]) * g(t0, X1_1)[:, 0] + (b1_2 * I1[0]+b2_2 * I2[1, 1] / sqrth) * g(t0 + c1_2 * h, X1_2)[:, 0] + (b1_3 * I1[0]+b2_3 * I2[1, 1] / sqrth) * g(t0 + c1_3 * h, X1_3)[:, 0] + (b1_1 * I1[1]) * g(t0, X2_1)[:, 1] + (b1_2 * I1[1]+b2_2 * I2[2, 2] / sqrth) * g(t0 + c1_2 * h, X2_2)[:, 1] + (b1_3 * I1[1]+b2_3 * I2[2, 2] / sqrth) * g(t0 + c1_3 * h, X2_3)[:, 1] + (b1_1 * I1[2]) * g(t0, X3_1)[:, 2] + (b1_2 * I1[2]+b2_2 * I2[3, 3] / sqrth) * g(t0 + c1_2 * h, X3_2)[:, 2] + (b1_3 * I1[2]+b2_3 * I2[3, 3] / sqrth) * g(t0 + c1_3 * h, X3_3)[:, 2] + (b1_1 * I1[3]) * g(t0, X4_1)[:, 3] + (b1_2 * I1[3]+b2_2 * I2[4, 4] / sqrth) * g(t0 + c1_2 * h, X4_2)[:, 3] + (b1_3 * I1[3]+b2_3 * I2[4, 4] / sqrth) * g(t0 + c1_3 * h, X4_3)[:, 3]+(b3_1 * I1[0]) * g(t0, Xhat1_1)[:, 0] + (b3_2 * I1[0]+b4_2 * sqrth) * g(t0, Xhat1_2)[:, 0] + (b3_3 * I1[0]+b4_3 * sqrth) * g(t0, Xhat1_3)[:, 0] + (b3_1 * I1[1]) * g(t0, Xhat2_1)[:, 1] + (b3_2 * I1[1]+b4_2 * sqrth) * g(t0, Xhat2_2)[:, 1] + (b3_3 * I1[1]+b4_3 * sqrth) * g(t0, Xhat2_3)[:, 1] + (b3_1 * I1[2]) * g(t0, Xhat3_1)[:, 2] + (b3_2 * I1[2]+b4_2 * sqrth) * g(t0, Xhat3_2)[:, 2] + (b3_3 * I1[2]+b4_3 * sqrth) * g(t0, Xhat3_3)[:, 2] + (b3_1 * I1[3]) * g(t0, Xhat4_1)[:, 3] + (b3_2 * I1[3]+b4_2 * sqrth) * g(t0, Xhat4_2)[:, 3] + (b3_3 * I1[3]+b4_3 * sqrth) * g(t0, Xhat4_3)[:, 3]        
        t0 = t0 + h

        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
        if not keep_only_last:
            xs.append(x)
        else:
            pass

    if keep_only_last:
        return x
    else:
        return xs


def weak_srk2w5(f: Callable[[float, np.ndarray], np.ndarray], g: Callable[[float, np.ndarray], np.ndarray], x_0: np.ndarray, wiener: WienerProcess, test_function: Callable[[np.ndarray], bool]=None, keep_only_last: bool=False) -> Union[List[np.ndarray], np.ndarray]:
    """Стохастический метод Рунге-Кутты со слабой сходимостью `ps=2.0`
    weak_srk2w5(f, g, x_0, wiener, last=False) -> List[ndarray[float]]

    Parameters
    ----------
    f : `function f(t: float, x: ndarray) -> ndarray`
        Векторная функция сноса (вектор сноса).
    g : `function G(t: float, x: ndarray) -> ndarray`
        Матричная функция диффузии (матрица диффузии).
    x_0 : array of float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: ndarray of float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of ndarray of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops.
    """
    # инициализация коэффициентов метода
    A0_21 = 1
    A1_21 = 1
    B0_21 = 1
    B1_21 = 1
    B2_21 = 1
    A1_31 = 1
    B1_31 = -1
    B2_31 = -1
    a_1 = 0.5
    a_2 = 0.5
    c0_2 = 1
    c1_2 = 1
    c1_3 = 1
    b1_1 = 0.5
    b1_2 = 0.25
    b1_3 = 0.25
    b2_2 = 0.5
    b2_3 = -0.5
    b3_1 = -0.5
    b3_2 = 0.25
    b3_3 = 0.25
    b4_2 = 0.5
    b4_3 = -0.5
    # Список для сохранения результатов
    xs = []
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt
    sqrth = np.sqrt(h)
    
    if not keep_only_last:
        xs.append(x)
    else:
        pass

    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
    
    # кратные интегралы Ито для векторного процесса
    Ito2 = wiener.ito_integral_2()

    for I1, I2 in zip(wiener.dx, Ito2):
        X0_1 = x
        X1_1 = x            
        X2_1 = x            
        X3_1 = x            
        X4_1 = x            
        X5_1 = x            
        
        Xhat1_1 = x            
        Xhat2_1 = x            
        Xhat3_1 = x            
        Xhat4_1 = x            
        Xhat5_1 = x            
        X0_2 = x + h * (A0_21 * f(t0, X0_1)) + B0_21 * g(t0, X1_1)[:, 0] * I1[0] + B0_21 * g(t0, X2_1)[:, 1] * I1[1] + B0_21 * g(t0, X3_1)[:, 2] * I1[2] + B0_21 * g(t0, X4_1)[:, 3] * I1[3] + B0_21 * g(t0, X5_1)[:, 4] * I1[4]
        X1_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X1_1)[:, 0] * sqrth            
        X2_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X2_1)[:, 1] * sqrth            
        X3_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X3_1)[:, 2] * sqrth            
        X4_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X4_1)[:, 3] * sqrth            
        X5_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X5_1)[:, 4] * sqrth            
        
        Xhat1_2 = x + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 1] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 1] / sqrth            
        Xhat2_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 2] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 2] / sqrth            
        Xhat3_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 3] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 3] / sqrth            
        Xhat4_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 4] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 4] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 4] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 4] / sqrth            
        Xhat5_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 5] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 5] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 5] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 5] / sqrth            
        X0_3 = x
        X1_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X1_1)[:, 0] * sqrth            
        X2_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X2_1)[:, 1] * sqrth            
        X3_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X3_1)[:, 2] * sqrth            
        X4_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X4_1)[:, 3] * sqrth            
        X5_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X5_1)[:, 4] * sqrth            
        
        Xhat1_3 = x + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 1] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 1] / sqrth            
        Xhat2_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 2] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 2] / sqrth            
        Xhat3_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 3] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 3] / sqrth            
        Xhat4_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 4] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 4] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 4] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 4] / sqrth            
        Xhat5_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 5] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 5] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 5] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 5] / sqrth            

        x = x + h * (a_1 * f(t0, X0_1) + a_2 * f(t0 + c0_2 * h, X0_2)) +(b1_1 * I1[0]) * g(t0, X1_1)[:, 0] + (b1_2 * I1[0]+b2_2 * I2[1, 1] / sqrth) * g(t0 + c1_2 * h, X1_2)[:, 0] + (b1_3 * I1[0]+b2_3 * I2[1, 1] / sqrth) * g(t0 + c1_3 * h, X1_3)[:, 0] + (b1_1 * I1[1]) * g(t0, X2_1)[:, 1] + (b1_2 * I1[1]+b2_2 * I2[2, 2] / sqrth) * g(t0 + c1_2 * h, X2_2)[:, 1] + (b1_3 * I1[1]+b2_3 * I2[2, 2] / sqrth) * g(t0 + c1_3 * h, X2_3)[:, 1] + (b1_1 * I1[2]) * g(t0, X3_1)[:, 2] + (b1_2 * I1[2]+b2_2 * I2[3, 3] / sqrth) * g(t0 + c1_2 * h, X3_2)[:, 2] + (b1_3 * I1[2]+b2_3 * I2[3, 3] / sqrth) * g(t0 + c1_3 * h, X3_3)[:, 2] + (b1_1 * I1[3]) * g(t0, X4_1)[:, 3] + (b1_2 * I1[3]+b2_2 * I2[4, 4] / sqrth) * g(t0 + c1_2 * h, X4_2)[:, 3] + (b1_3 * I1[3]+b2_3 * I2[4, 4] / sqrth) * g(t0 + c1_3 * h, X4_3)[:, 3] + (b1_1 * I1[4]) * g(t0, X5_1)[:, 4] + (b1_2 * I1[4]+b2_2 * I2[5, 5] / sqrth) * g(t0 + c1_2 * h, X5_2)[:, 4] + (b1_3 * I1[4]+b2_3 * I2[5, 5] / sqrth) * g(t0 + c1_3 * h, X5_3)[:, 4]+(b3_1 * I1[0]) * g(t0, Xhat1_1)[:, 0] + (b3_2 * I1[0]+b4_2 * sqrth) * g(t0, Xhat1_2)[:, 0] + (b3_3 * I1[0]+b4_3 * sqrth) * g(t0, Xhat1_3)[:, 0] + (b3_1 * I1[1]) * g(t0, Xhat2_1)[:, 1] + (b3_2 * I1[1]+b4_2 * sqrth) * g(t0, Xhat2_2)[:, 1] + (b3_3 * I1[1]+b4_3 * sqrth) * g(t0, Xhat2_3)[:, 1] + (b3_1 * I1[2]) * g(t0, Xhat3_1)[:, 2] + (b3_2 * I1[2]+b4_2 * sqrth) * g(t0, Xhat3_2)[:, 2] + (b3_3 * I1[2]+b4_3 * sqrth) * g(t0, Xhat3_3)[:, 2] + (b3_1 * I1[3]) * g(t0, Xhat4_1)[:, 3] + (b3_2 * I1[3]+b4_2 * sqrth) * g(t0, Xhat4_2)[:, 3] + (b3_3 * I1[3]+b4_3 * sqrth) * g(t0, Xhat4_3)[:, 3] + (b3_1 * I1[4]) * g(t0, Xhat5_1)[:, 4] + (b3_2 * I1[4]+b4_2 * sqrth) * g(t0, Xhat5_2)[:, 4] + (b3_3 * I1[4]+b4_3 * sqrth) * g(t0, Xhat5_3)[:, 4]        
        t0 = t0 + h

        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
        if not keep_only_last:
            xs.append(x)
        else:
            pass

    if keep_only_last:
        return x
    else:
        return xs


def weak_srk2w6(f: Callable[[float, np.ndarray], np.ndarray], g: Callable[[float, np.ndarray], np.ndarray], x_0: np.ndarray, wiener: WienerProcess, test_function: Callable[[np.ndarray], bool]=None, keep_only_last: bool=False) -> Union[List[np.ndarray], np.ndarray]:
    """Стохастический метод Рунге-Кутты со слабой сходимостью `ps=2.0`
    weak_srk2w6(f, g, x_0, wiener, last=False) -> List[ndarray[float]]

    Parameters
    ----------
    f : `function f(t: float, x: ndarray) -> ndarray`
        Векторная функция сноса (вектор сноса).
    g : `function G(t: float, x: ndarray) -> ndarray`
        Матричная функция диффузии (матрица диффузии).
    x_0 : array of float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: ndarray of float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of ndarray of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops.
    """
    # инициализация коэффициентов метода
    A0_21 = 1
    A1_21 = 1
    B0_21 = 1
    B1_21 = 1
    B2_21 = 1
    A1_31 = 1
    B1_31 = -1
    B2_31 = -1
    a_1 = 0.5
    a_2 = 0.5
    c0_2 = 1
    c1_2 = 1
    c1_3 = 1
    b1_1 = 0.5
    b1_2 = 0.25
    b1_3 = 0.25
    b2_2 = 0.5
    b2_3 = -0.5
    b3_1 = -0.5
    b3_2 = 0.25
    b3_3 = 0.25
    b4_2 = 0.5
    b4_3 = -0.5
    # Список для сохранения результатов
    xs = []
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt
    sqrth = np.sqrt(h)
    
    if not keep_only_last:
        xs.append(x)
    else:
        pass

    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
    
    # кратные интегралы Ито для векторного процесса
    Ito2 = wiener.ito_integral_2()

    for I1, I2 in zip(wiener.dx, Ito2):
        X0_1 = x
        X1_1 = x            
        X2_1 = x            
        X3_1 = x            
        X4_1 = x            
        X5_1 = x            
        X6_1 = x            
        
        Xhat1_1 = x            
        Xhat2_1 = x            
        Xhat3_1 = x            
        Xhat4_1 = x            
        Xhat5_1 = x            
        Xhat6_1 = x            
        X0_2 = x + h * (A0_21 * f(t0, X0_1)) + B0_21 * g(t0, X1_1)[:, 0] * I1[0] + B0_21 * g(t0, X2_1)[:, 1] * I1[1] + B0_21 * g(t0, X3_1)[:, 2] * I1[2] + B0_21 * g(t0, X4_1)[:, 3] * I1[3] + B0_21 * g(t0, X5_1)[:, 4] * I1[4] + B0_21 * g(t0, X6_1)[:, 5] * I1[5]
        X1_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X1_1)[:, 0] * sqrth            
        X2_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X2_1)[:, 1] * sqrth            
        X3_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X3_1)[:, 2] * sqrth            
        X4_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X4_1)[:, 3] * sqrth            
        X5_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X5_1)[:, 4] * sqrth            
        X6_2 = x + h * (A1_21 * f(t0, X0_1)) + B1_21 * g(t0, X6_1)[:, 5] * sqrth            
        
        Xhat1_2 = x + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 1] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 1] / sqrth + B2_21 * g(t0, X6_1)[:, 5] * I2[6, 1] / sqrth            
        Xhat2_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 2] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 2] / sqrth + B2_21 * g(t0, X6_1)[:, 5] * I2[6, 2] / sqrth            
        Xhat3_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 3] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 3] / sqrth + B2_21 * g(t0, X6_1)[:, 5] * I2[6, 3] / sqrth            
        Xhat4_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 4] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 4] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 4] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 4] / sqrth + B2_21 * g(t0, X6_1)[:, 5] * I2[6, 4] / sqrth            
        Xhat5_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 5] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 5] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 5] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 5] / sqrth + B2_21 * g(t0, X6_1)[:, 5] * I2[6, 5] / sqrth            
        Xhat6_2 = x + B2_21 * g(t0, X1_1)[:, 0] * I2[1, 6] / sqrth + B2_21 * g(t0, X2_1)[:, 1] * I2[2, 6] / sqrth + B2_21 * g(t0, X3_1)[:, 2] * I2[3, 6] / sqrth + B2_21 * g(t0, X4_1)[:, 3] * I2[4, 6] / sqrth + B2_21 * g(t0, X5_1)[:, 4] * I2[5, 6] / sqrth            
        X0_3 = x
        X1_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X1_1)[:, 0] * sqrth            
        X2_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X2_1)[:, 1] * sqrth            
        X3_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X3_1)[:, 2] * sqrth            
        X4_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X4_1)[:, 3] * sqrth            
        X5_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X5_1)[:, 4] * sqrth            
        X6_3 = x + h * (A1_31 * f(t0, X0_1)) + B1_31 * g(t0, X6_1)[:, 5] * sqrth            
        
        Xhat1_3 = x + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 1] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 1] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 1] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 1] / sqrth + B2_31 * g(t0, X6_1)[:, 5] * I2[6, 1] / sqrth            
        Xhat2_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 2] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 2] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 2] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 2] / sqrth + B2_31 * g(t0, X6_1)[:, 5] * I2[6, 2] / sqrth            
        Xhat3_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 3] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 3] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 3] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 3] / sqrth + B2_31 * g(t0, X6_1)[:, 5] * I2[6, 3] / sqrth            
        Xhat4_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 4] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 4] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 4] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 4] / sqrth + B2_31 * g(t0, X6_1)[:, 5] * I2[6, 4] / sqrth            
        Xhat5_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 5] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 5] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 5] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 5] / sqrth + B2_31 * g(t0, X6_1)[:, 5] * I2[6, 5] / sqrth            
        Xhat6_3 = x + B2_31 * g(t0, X1_1)[:, 0] * I2[1, 6] / sqrth + B2_31 * g(t0, X2_1)[:, 1] * I2[2, 6] / sqrth + B2_31 * g(t0, X3_1)[:, 2] * I2[3, 6] / sqrth + B2_31 * g(t0, X4_1)[:, 3] * I2[4, 6] / sqrth + B2_31 * g(t0, X5_1)[:, 4] * I2[5, 6] / sqrth            

        x = x + h * (a_1 * f(t0, X0_1) + a_2 * f(t0 + c0_2 * h, X0_2)) +(b1_1 * I1[0]) * g(t0, X1_1)[:, 0] + (b1_2 * I1[0]+b2_2 * I2[1, 1] / sqrth) * g(t0 + c1_2 * h, X1_2)[:, 0] + (b1_3 * I1[0]+b2_3 * I2[1, 1] / sqrth) * g(t0 + c1_3 * h, X1_3)[:, 0] + (b1_1 * I1[1]) * g(t0, X2_1)[:, 1] + (b1_2 * I1[1]+b2_2 * I2[2, 2] / sqrth) * g(t0 + c1_2 * h, X2_2)[:, 1] + (b1_3 * I1[1]+b2_3 * I2[2, 2] / sqrth) * g(t0 + c1_3 * h, X2_3)[:, 1] + (b1_1 * I1[2]) * g(t0, X3_1)[:, 2] + (b1_2 * I1[2]+b2_2 * I2[3, 3] / sqrth) * g(t0 + c1_2 * h, X3_2)[:, 2] + (b1_3 * I1[2]+b2_3 * I2[3, 3] / sqrth) * g(t0 + c1_3 * h, X3_3)[:, 2] + (b1_1 * I1[3]) * g(t0, X4_1)[:, 3] + (b1_2 * I1[3]+b2_2 * I2[4, 4] / sqrth) * g(t0 + c1_2 * h, X4_2)[:, 3] + (b1_3 * I1[3]+b2_3 * I2[4, 4] / sqrth) * g(t0 + c1_3 * h, X4_3)[:, 3] + (b1_1 * I1[4]) * g(t0, X5_1)[:, 4] + (b1_2 * I1[4]+b2_2 * I2[5, 5] / sqrth) * g(t0 + c1_2 * h, X5_2)[:, 4] + (b1_3 * I1[4]+b2_3 * I2[5, 5] / sqrth) * g(t0 + c1_3 * h, X5_3)[:, 4] + (b1_1 * I1[5]) * g(t0, X6_1)[:, 5] + (b1_2 * I1[5]+b2_2 * I2[6, 6] / sqrth) * g(t0 + c1_2 * h, X6_2)[:, 5] + (b1_3 * I1[5]+b2_3 * I2[6, 6] / sqrth) * g(t0 + c1_3 * h, X6_3)[:, 5]+(b3_1 * I1[0]) * g(t0, Xhat1_1)[:, 0] + (b3_2 * I1[0]+b4_2 * sqrth) * g(t0, Xhat1_2)[:, 0] + (b3_3 * I1[0]+b4_3 * sqrth) * g(t0, Xhat1_3)[:, 0] + (b3_1 * I1[1]) * g(t0, Xhat2_1)[:, 1] + (b3_2 * I1[1]+b4_2 * sqrth) * g(t0, Xhat2_2)[:, 1] + (b3_3 * I1[1]+b4_3 * sqrth) * g(t0, Xhat2_3)[:, 1] + (b3_1 * I1[2]) * g(t0, Xhat3_1)[:, 2] + (b3_2 * I1[2]+b4_2 * sqrth) * g(t0, Xhat3_2)[:, 2] + (b3_3 * I1[2]+b4_3 * sqrth) * g(t0, Xhat3_3)[:, 2] + (b3_1 * I1[3]) * g(t0, Xhat4_1)[:, 3] + (b3_2 * I1[3]+b4_2 * sqrth) * g(t0, Xhat4_2)[:, 3] + (b3_3 * I1[3]+b4_3 * sqrth) * g(t0, Xhat4_3)[:, 3] + (b3_1 * I1[4]) * g(t0, Xhat5_1)[:, 4] + (b3_2 * I1[4]+b4_2 * sqrth) * g(t0, Xhat5_2)[:, 4] + (b3_3 * I1[4]+b4_3 * sqrth) * g(t0, Xhat5_3)[:, 4] + (b3_1 * I1[5]) * g(t0, Xhat6_1)[:, 5] + (b3_2 * I1[5]+b4_2 * sqrth) * g(t0, Xhat6_2)[:, 5] + (b3_3 * I1[5]+b4_3 * sqrth) * g(t0, Xhat6_3)[:, 5]        
        t0 = t0 + h

        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
        if not keep_only_last:
            xs.append(x)
        else:
            pass

    if keep_only_last:
        return x
    else:
        return xs
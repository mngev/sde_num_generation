#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""Module for stochastic processes generation: Wiener and Poisson"""
import numpy as np

from typing import Tuple


class StochasticProcess(object):
    """Parent class for scalar stochastic processes"""
    def __init__(self, time_interval=(0.0, 1.0), seed=None, **kwargs):
        """StochasticProcess class constructor.

        Parameters
        ----------
        time_interval : tuple of floats, optional
            Time interval (t_start, t_stop).
        seed : integer, optional
            Random generator seed value.
        h : float, optional
            Step size (it is possible to set h or N, but not both).
        N : int, optional
            Number of steps in process trajectory.
        dim: int or tuple of int
            Number of dimensions of stochastic process (default is scalar, dim=1).
        """
        if seed:
            np.random.seed(seed)

        self.t_0, self.T = time_interval

        if 'h' in kwargs:
            self.dt = kwargs.pop('h')
            self.N = int((self.T - self.t_0)/self.dt)
        elif 'N' in kwargs:
            self.N = kwargs.pop('N')
            self.dt = (self.T - self.t_0)/float(self.N)
        else:
            self.N = 1000
            self.dt = (self.T - self.t_0)/float(self.N)

        if 'dim' in kwargs:
            self.dim = kwargs.pop('dim')
        else:
            self.dim = 1

        self.t = np.arange(self.t_0, self.T+self.dt, self.dt)
        # scalar process
        if self.dim == 1:
            self.x = np.empty(shape=self.N+1)
        # multidimensional process
        else:
            self.x = np.empty(shape=(self.N+1, self.dim))


class WienerProcess(StochasticProcess):
    """Scalar or multidimensional Wiener stochastic process"""
    def __init__(self, time_interval=(0.0, 1.0), seed=None, **kwargs):
        """Wiener process class constructor.

        Parameters
        ----------
        time_interval : tuple of floats, optional
            Interval of the process evolution.
        seed : integer, optional
            Optional seed value for the random generator.
        h : float, optional
            The step size (it is possible to set or `h`, or `N`, but not both).
        N : int, optional
            The number of steps in process trajectory.
        dim : int or tuple of int
            The number of dimensions of Wiener process (default is scalar, `dim=1`).

        Other Parameters
        ----------------
        x_0 : array_like, optional
            start value, default is `0`.
        scale : float, optional
            scale of normal distribution (for dW values).
        """
        super(WienerProcess, self).__init__(time_interval, seed, **kwargs)

        if 'x_0' in kwargs:
            self.x[0] = kwargs.pop('x_0')
        else:
            self.x[0] = 0.0

        # если нужно уменьшить мат. ожидание
        if 'scale' in kwargs:
            scale = kwargs.pop('scale') * np.sqrt(self.dt)
        else:
            scale = np.sqrt(self.dt)

        if self.dim > 1:
            self.dx = np.random.normal(loc=0.0, scale=scale, size=(self.N, self.dim))
        else:
            self.dx = np.random.normal(loc=0.0, scale=scale, size=self.N)

        self.x[1:] = np.cumsum(self.dx, axis=0) + self.x[0]

    def scalar_ito_integrals(self, seed=None) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        """Calculate four scalar Ito integrals for given Wiener stochastic process

        Parameters
        ----------
        seed : int (optional)
            Seed for random generator.

        Returns
        -------
        ito1 : 1-d ndarray of floats
            I^1(h_n) Ito integrals for h_1, h_2,...,h_n.
        ito10 : 1-d ndarray of floats
            I^{10}(h_n) Ito integrals for h_1, h_2,...,h_n.
        ito11 : 1-d ndarray of floats
            I^{11}(h_n) Ito integrals for h_1, h_2,...,h_n.
        ito111 : 1-d ndarray of floats
            I^{111}(h_n) Ito integrals for h_1, h_2,...,h_n.

        Raises
        ------
        ValueError
            If Wiener process is not scalar
        """
        if seed:
            np.random.seed(seed)
        if self.dim != 1:
            raise ValueError('Wiener process has to be scalar!')
        ζ = np.random.normal(size=self.N, scale=np.sqrt(self.dt))
        ito1 = self.dx
        ito10 = 0.5 * self.dt * (self.dx + ζ / np.sqrt(3.0))
        ito11 = 0.5 * (self.dx**2 - self.dt)
        # проверить эту формулу! (вроде бы верна)
        ito111 = (1.0/6.0) * (ito1**3 - 3 * self.dt * self.dx)
        return ito1, ito10, ito11, ito111

    def ito_integral_1(self) -> Tuple[float, np.ndarray]:
        """One dimension stochastic Ito integral

        Returns
        -------
        h : float
            Step of process discretisation
        dW : 1-d or 2-d ndarray
            Wiener process increments
        """
        return self.dt, self.dx

    def ito_integral_2(self, iterations_num=30, seed=None) -> np.ndarray:
        """Two dimensions stochastic Ito integral

        Returns
        -------
        ito : 3-d ndarray
            This array consists of four parts:
            - ito[0:N, 0, 0] --- I^{00}(h_n) Ito integral,
            - ito[0:N, 1:, 0] --- I^{α,0}(h_n), α = 1,2,…,m  Ito integral,
            - ito[0:N, 0, 1:] --- I^{0,α}(h_n), α = 1,2,…,m  Ito integral,
            - ito[0:N, 1:, 1:] --- I^{α,β}(h_n), α,β = 1,2,…,m  Ito integral.
        """
        if seed:
            np.random.seed(seed)

        ito = np.empty(shape=(self.N, self.dim+1, self.dim+1))
        # I^{00}(h_n)
        ito[:, 0, 0] = 0.5 * self.dt**2
        #  I^{α,0}(h_n), α = 1,2,…,m
        ζ = np.random.normal(scale=np.sqrt(self.dt), size=(self.N, self.dim))
        ito[:, 1:, 0] = 0.5 * self.dt * (self.dx + ζ/np.sqrt(3.0))
        #  I^{0,α}(h_n), α = 1,2,…,m
        ζ = np.random.normal(scale=np.sqrt(self.dt), size=(self.N, self.dim))
        ito[:, 0, 1:] = 0.5 * self.dt * (self.dx - ζ/np.sqrt(3.0))

        # I^{α,β}(h_n), α,β = 1,2,…,m
        h = np.sqrt(2.0/self.dt)
        I = np.identity(self.dim)
        # Слабое место по производительности. Здесь мы проходимся по всем инкрементам
        # винеровского процесса
        for i, dw in enumerate(self.dx):
            # бесконечная сумма для аппроксимации интеграла Ито
            A = np.zeros(shape=(self.dim, self.dim))
            for k in range(1, iterations_num):
                X = np.random.normal(loc=0.0, scale=1.0, size=self.dim)
                Y = np.random.normal(loc=0.0, scale=1.0, size=self.dim) + h*dw
                A += (np.outer(X, Y) - np.outer(Y, X)) / float(k)

            ito[i, 1:, 1:] = 0.5*(np.outer(dw, dw) - self.dt * I) + 0.5*self.dt * A / np.pi

        return ito


class PoissonProcess(StochasticProcess):
    """Poisson stochastic process """
    def __init__(self, time_interval=(0.0, 1.0), seed=None, **kwargs):
        """The Poisson distribution P{X=k} = [(λ*dt)**k * exp(-λ*dt)]/(k!)

        Parameters
        ----------
        time_interval : tuple of float, optional
            interval of process evolution
        seed : int, optional
            optional seed value for random generator
        h : float, optional
            step size (it is possible to set h or N, but not both),
        N : int, optional
            number of steps in Poisson process trajectory,
        dim : int or tuple of int, optional
            the number of dimensions of Poisson process (default is scalar, `dim=1`).
        Lambda : float, optional
            Poisson distribution parameter (default is 1)
        """
        super(PoissonProcess, self).__init__(time_interval, seed, **kwargs)
        self.x[0] = 0.0
        self.Lambda = kwargs.pop('Lambda') if 'Lambda' in kwargs else 1.0
        self.dx = np.random.poisson(lam=self.Lambda*self.dt, size=self.N)
        self.x[1:] = np.cumsum(self.dx)

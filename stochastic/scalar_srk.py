from .stochasticprocess import WienerProcess
from typing import Callable, List, Union
import numpy as np


def strong_srk1w1(f: Callable[[float, float], float], g: Callable[[float, float], float], x_0: float, wiener: WienerProcess, test_function: Callable[[float], bool]=None, keep_only_last=False) -> Union[float, List[float]]:
    """Численный метод для скалярного СДУ
    
    Parameters
    ----------
    f : `function f(t, x) -> float`
        Скалярная функция сноса (вектор сноса для скалярного случая).
    g : `function G(t, x) -> float`
        Функция диффузии (матрица диффузии для скалярного случая).
    x_0 : float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops."""
    # Список для сохранения результатов
    xs = []
    # кратные интегралы Ито для скалярного процесса
    ito1, ito10, ito11, ito111 = wiener.scalar_ito_integrals()
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt
    # инициализация коэффициентов метода
    A0_21 = 0.75
    A1_21 = 0.25
    B0_21 = 1.5
    B1_21 = 0.5
    A1_31 = 1
    B1_31 = -1
    A1_43 = 0.25
    B1_41 = -5
    B1_42 = 3
    B1_43 = 0.5
    a_1 = 0.33333333333333331
    a_2 = 0.66666666666666663
    c0_2 = 0.75
    c1_2 = 0.25
    c1_3 = 1
    c1_4 = 0.25
    b1_1 = -1
    b1_2 = 1.3333333333333333
    b1_3 = 0.66666666666666663
    b2_1 = -1
    b2_2 = 1.3333333333333333
    b2_3 = -0.33333333333333331
    b3_1 = 2
    b3_2 = -1.3333333333333333
    b3_3 = -0.66666666666666663
    b4_1 = -2
    b4_2 = 1.6666666666666667
    b4_3 = -0.66666666666666663
    b4_4 = 1
    # Включаем начальную точку в результат
    if not keep_only_last:
        xs.append(x)
    
    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
        
    for i1, i10, i11, i111 in zip(ito1, ito10, ito11, ito111):
        # шаг метода
        X0_1 = x            
        X1_1 = x            
        X0_2 = x + h*(A0_21 * f(t0, X0_1)) + (i10 / np.sqrt(h)) * (B0_21 * g(t0, X1_1))            
        X1_2 = x + h*(A1_21 * f(t0, X0_1)) + np.sqrt(h) * (B1_21 * g(t0, X1_1))            
        X0_3 = x            
        X1_3 = x + h*(A1_31 * f(t0, X0_1)) + np.sqrt(h) * (B1_31 * g(t0, X1_1))            
        X0_4 = x            
        X1_4 = x + h*(A1_43 * f(t0, X0_3)) + np.sqrt(h) * (B1_41 * g(t0, X1_1) + B1_42 * g(t0 + c1_2 * h, X1_2) + B1_43 * g(t0 + c1_3 * h, X1_3))            

        x = x + h * (a_1 * f(t0, X0_1) + a_2 * f(t0 + c0_2 * h, X0_2))+(b1_1 * i1+ b2_1 * i11 / np.sqrt(h)+ b3_1 * i10 / h+ b4_1 * i111 / h) * g(t0, X1_1) + (b1_2 * i1+ b2_2 * i11 / np.sqrt(h)+ b3_2 * i10 / h+ b4_2 * i111 / h) * g(t0 + c1_2 * h, X1_2) + (b1_3 * i1+ b2_3 * i11 / np.sqrt(h)+ b3_3 * i10 / h+ b4_3 * i111 / h) * g(t0 + c1_3 * h, X1_3) + (b4_4 * i111 / h) * g(t0 + c1_4 * h, X1_4)        
        t0 = t0 + h
        
        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
            
        if not keep_only_last:
            xs.append(x)
        else:
            pass
    if keep_only_last:
        return x
    else:
        return xs


def strong_srk2w1(f: Callable[[float, float], float], g: Callable[[float, float], float], x_0: float, wiener: WienerProcess, test_function: Callable[[float], bool]=None, keep_only_last=False) -> Union[float, List[float]]:
    """Численный метод для скалярного СДУ
    
    Parameters
    ----------
    f : `function f(t, x) -> float`
        Скалярная функция сноса (вектор сноса для скалярного случая).
    g : `function G(t, x) -> float`
        Функция диффузии (матрица диффузии для скалярного случая).
    x_0 : float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops."""
    # Список для сохранения результатов
    xs = []
    # кратные интегралы Ито для скалярного процесса
    ito1, ito10, ito11, ito111 = wiener.scalar_ito_integrals()
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt
    # инициализация коэффициентов метода
    A0_21 = 1
    A1_21 = 0.25
    B1_21 = -0.5
    A0_31 = 0.25
    A0_32 = 0.25
    A1_31 = 1
    B0_31 = 1
    B0_32 = 0.5
    B1_31 = 1
    A1_43 = 0.25
    B1_41 = 2
    B1_42 = -1
    B1_43 = 0.5
    a_1 = 0.16666666666666666
    a_2 = 0.16666666666666666
    a_3 = 0.66666666666666663
    c0_2 = 1
    c0_3 = 0.5
    c1_2 = 0.25
    c1_3 = 1
    c1_4 = 0.25
    b1_1 = -1
    b1_2 = 1.3333333333333333
    b1_3 = 0.66666666666666663
    b2_1 = 1
    b2_2 = -1.3333333333333333
    b2_3 = 0.33333333333333331
    b3_1 = 2
    b3_2 = -1.3333333333333333
    b3_3 = -0.66666666666666663
    b4_1 = -2
    b4_2 = 1.6666666666666667
    b4_3 = -0.66666666666666663
    b4_4 = 1
    # Включаем начальную точку в результат
    if not keep_only_last:
        xs.append(x)
    
    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
        
    for i1, i10, i11, i111 in zip(ito1, ito10, ito11, ito111):
        # шаг метода
        X0_1 = x            
        X1_1 = x            
        X0_2 = x + h*(A0_21 * f(t0, X0_1))            
        X1_2 = x + h*(A1_21 * f(t0, X0_1)) + np.sqrt(h) * (B1_21 * g(t0, X1_1))            
        X0_3 = x + h*(A0_31 * f(t0, X0_1) + A0_32 * f(t0 + c0_2 * h, X0_2)) + (i10 / np.sqrt(h)) * (B0_31 * g(t0, X1_1) + B0_32 * g(t0 + c1_2 * h, X1_2))            
        X1_3 = x + h*(A1_31 * f(t0, X0_1)) + np.sqrt(h) * (B1_31 * g(t0, X1_1))            
        X0_4 = x            
        X1_4 = x + h*(A1_43 * f(t0 + c0_3 * h, X0_3)) + np.sqrt(h) * (B1_41 * g(t0, X1_1) + B1_42 * g(t0 + c1_2 * h, X1_2) + B1_43 * g(t0 + c1_3 * h, X1_3))            

        x = x + h * (a_1 * f(t0, X0_1) + a_2 * f(t0 + c0_2 * h, X0_2) + a_3 * f(t0 + c0_3 * h, X0_3))+(b1_1 * i1+ b2_1 * i11 / np.sqrt(h)+ b3_1 * i10 / h+ b4_1 * i111 / h) * g(t0, X1_1) + (b1_2 * i1+ b2_2 * i11 / np.sqrt(h)+ b3_2 * i10 / h+ b4_2 * i111 / h) * g(t0 + c1_2 * h, X1_2) + (b1_3 * i1+ b2_3 * i11 / np.sqrt(h)+ b3_3 * i10 / h+ b4_3 * i111 / h) * g(t0 + c1_3 * h, X1_3) + (b4_4 * i111 / h) * g(t0 + c1_4 * h, X1_4)        
        t0 = t0 + h
        
        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
            
        if not keep_only_last:
            xs.append(x)
        else:
            pass
    if keep_only_last:
        return x
    else:
        return xs


def strong_KlPl(f: Callable[[float, float], float], g: Callable[[float, float], float], x_0: float, wiener: WienerProcess, test_function: Callable[[float], bool]=None, keep_only_last=False) -> Union[float, List[float]]:
    """Численный метод для скалярного СДУ
    
    Parameters
    ----------
    f : `function f(t, x) -> float`
        Скалярная функция сноса (вектор сноса для скалярного случая).
    g : `function G(t, x) -> float`
        Функция диффузии (матрица диффузии для скалярного случая).
    x_0 : float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops."""
    # Список для сохранения результатов
    xs = []
    # кратные интегралы Ито для скалярного процесса
    ito1, ito10, ito11, ito111 = wiener.scalar_ito_integrals()
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt
    # инициализация коэффициентов метода
    A1_21 = 1
    B1_21 = 1
    a_1 = 1
    b1_1 = 1
    b2_1 = -1
    b2_2 = 1
    b3_1 = 1
    # Включаем начальную точку в результат
    if not keep_only_last:
        xs.append(x)
    
    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
        
    for i1, i10, i11, i111 in zip(ito1, ito10, ito11, ito111):
        # шаг метода
        X0_1 = x            
        X1_1 = x            
        X0_2 = x            
        X1_2 = x + h*(A1_21 * f(t0, X0_1)) + np.sqrt(h) * (B1_21 * g(t0, X1_1))            

        x = x + h * (a_1 * f(t0, X0_1))+(b1_1 * i1+ b2_1 * i11 / np.sqrt(h)+ b3_1 * i10 / h) * g(t0, X1_1) + (b2_2 * i11 / np.sqrt(h)) * g(t0, X1_2)        
        t0 = t0 + h
        
        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
            
        if not keep_only_last:
            xs.append(x)
        else:
            pass
    if keep_only_last:
        return x
    else:
        return xs
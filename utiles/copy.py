# -*- coding: utf-8 -*-

"""Скрипт предназначен для копирования файлов модуля stochastic
во все директории, где им пользуются другие программы"""


import shutil
import os.path

MODULE_PATH = '../stochastic'

# Директория с Jupyter блокнотами
DESTINATION_01 = '../../../iPyNotebook/Стохастические_процессы/stochastic'

FILE_01 = '../../../iPyNotebook/Стохастические_процессы/parallel.py'

OLD_PYCACHE = os.path.join(DESTINATION_01, '__pycache__')

# Предварительно удаляем все старые файлы
if os.path.exists(DESTINATION_01):
    shutil.rmtree(DESTINATION_01)

# Копируем новые
shutil.copytree(src=MODULE_PATH, dst=DESTINATION_01)

shutil.copy('../tests/parallel.py', FILE_01)

# Чистим `__pycache__` файлы
if os.path.exists(OLD_PYCACHE):
    shutil.rmtree(OLD_PYCACHE)

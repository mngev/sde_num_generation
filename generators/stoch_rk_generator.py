from jinja2 import Environment, FileSystemLoader, select_autoescape
from coefficients_table import ScalarMethod, StrongVectorMethod, WeakVectorMethod

# максимальная размерность системы уравнений, для которой надо сгенерировать код
MAX_EQN_DIM = 6

# Указываем директорию с шаблонами
env = Environment(loader=FileSystemLoader('templates'), trim_blocks=True, lstrip_blocks=True)
# Считываем таблицы коэффициентов и другую информацию о стохастических методах
# Рунге-Кутты трех видов: сильных скалярных, сильных векторных и слабых векторных
scalar_methods = ScalarMethod.from_json('tables/scalar.json')
strong_methods = StrongVectorMethod.from_json('tables/strong.json')
weak_methods = WeakVectorMethod.from_json('tables/weak.json')

# в этот список будем заносить сгенерированных код функций
funcs_src = []

# перебираем вначале все скалярные методы
for method in scalar_methods:
    src = env.get_template('scalar_srk.py')
    print("Генерируем метод:", method.name)
    # A0, A1, B0, B1, a, c0, c1, b1, b2, b3, b4
    func_src = src.render(function_name=method.name, stage=method.s, A0=method.A0, A1=method.A1,
                          B0=method.B0, B1=method.B1, a=method.a, c0=method.c0, c1=method.c1,
                          b1=method.b1, b2=method.b2, b3=method.b3, b4=method.b4)
    funcs_src.append(func_src)

funcs_src = '\n\n\n'.join(funcs_src)
# открываем файл с шаблоном модуля, чтобы записать в него все сгенерированные
# выше скалярные функции
src = env.get_template('scalar_srk_module.py')

# сохраняем весь получившийся код в файл, находящийся уровнем выше в директории stochastic
with open('../stochastic/scalar_srk.py', 'w', encoding='utf-8') as file1:
    file1.write(src.render(functions=funcs_src))
file1.close()

# повторяем ту же последовательность действий для векторного метода с сильной сходимостью
funcs_src = []

for method in strong_methods:
    src = env.get_template('strong_srk.py')
    print("Генерируем метод:", method.name)
    # A0, A1, B0, B1, a, c0, c1, b1, b2
    #  мы гененируем методы для двумерного процесса Винера. В случае, если размерность
    # должна быть больше, необходимо изменить переменную m
    func_src = [src.render(function_name=method.name, m=dim, stage=method.s, A0=method.A0,
                          A1=method.A1, B0=method.B0, B1=method.B1, a=method.a, c0=method.c0,
                          c1=method.c1,  b1=method.b1, b2=method.b2) for dim in range(2, MAX_EQN_DIM+1)]
    funcs_src.extend(func_src)

funcs_src = '\n\n\n'.join(funcs_src)

src = env.get_template('strong_srk_module.py')

with open('../stochastic/strong_srk.py', 'w', encoding='utf-8') as file2:
    file2.write(src.render(functions=funcs_src))

file2.close()


# в третий раз повторяем ту же последовательность действий для векторного метода со слабой сходимостью
funcs_src = []

for method in weak_methods:
    src = env.get_template('weak_srk.py')
    print("Генерируем метод:", method.name)
    # A0, A1, A2, B0, B1, B2, B1, a, c0, c1, b1, b2, b3, b4
    #  мы генерируем методы для двумерного процесса Винера. В случае, если размерность
    # должна быть больше, необходимо изменить переменную m
    func_src = [src.render(function_name=method.name, m=dim, stage=method.s, A0=method.A0,
                           A1=method.A1, A2 = method.A2, B0=method.B0, B1=method.B1, B2=method.B2,
                           a=method.a, c0=method.c0, c1=method.c1, c2 = method.c2, b1=method.b1,
                           b2=method.b2, b3=method.b3, b4=method.b4) for dim in range(2, MAX_EQN_DIM+1)]
    funcs_src.extend(func_src)

funcs_src = '\n\n\n'.join(funcs_src)

src = env.get_template('weak_srk_module.py')

with open('../stochastic/weak_srk.py', 'w', encoding='utf-8') as file3:
    file3.write(src.render(functions=funcs_src))

file3.close()

# ---------------------------------------------
#           генерация LaTeX формул
# ---------------------------------------------

# генерируем LaTeX формулы векторного метода с сильной сходимостью в общем виде
tex = env.get_template('strong_srk_general.tex')

with open('latex/storng_srk.tex', 'w', encoding='utf-8') as latex_file:
    latex_file.write(tex.render(stage=3, m=2))

funcs_src = []
# теперь генерируем LaTeX формулы для конкретных методов, перечисленных в JSON файлах
for method in strong_methods:
    src = env.get_template('strong_srk.tex')
    # A0, A1, B0, B1, a, c0, c1, b1, b2
    func_src = src.render(name=method.name, m=2, stage=method.s, A0=method.A0,
                          A1=method.A1, B0=method.B0, B1=method.B1, a=method.a, c0=method.c0,
                          c1=method.c1,  b1=method.b1, b2=method.b2)
    funcs_src.append(func_src)

with open('latex/storng_srk.tex', 'a', encoding='utf-8') as latex_file:
    latex_file.write('\n'.join(funcs_src))

latex_file.close()

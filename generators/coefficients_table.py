import numpy as np

from typing import Union, Iterable
from fractions import Fraction
import json as json


def fraction_to_latex(rational: Fraction, compact=True) -> str:
    """ fraction_to_latex(rational: Fraction, compact=True) -> str
    Convert the rational fraction to string like 1/2
    or LaTeX format `\frac{numerator}{denominator}`
    :param rational: rational number like Fraction(1, 2)
    :param compact: set False for LaTeX format fraction, default is True
    :return: string like '1/2' or '\frac{1}{2}'
    """
    r = Fraction(rational)
    if r.numerator == 0:
        res = '0'.format(r.numerator, r.denominator)
    elif r.denominator == 1:
        res = '{0:d}'.format(r.numerator, r.denominator)
    else:
        if compact:
            res = '{0:d}/{1:d}'.format(r.numerator, r.denominator)
        else:
            res = r'\frac{{{0:d}}}{{{1:d}}}'.format(r.numerator, r.denominator)
    return res


# Convert array or list of fraction to string or LaTeX format
fractions_list_to_latex = np.vectorize(fraction_to_latex, otypes=[np.str])


def array_from_list(coefficients: Union[Iterable[str], Iterable[float]]) -> np.ndarray:
    """Convert list of strings fractions"""
    res = np.array(coefficients)
    if res.dtype.type == np.str_:
        # Векторизируем конструктор рациональной дроби.
        res = np.vectorize(Fraction)(res)
    else:
        pass
    return res


class CoefficientsTable(object):
    """The parent class for coefficients tables classes of
    Runge-Kutta-like stochastic numerical methods.
    Родительский класс для остальных классов, которые должны реализовать частные
    случаи таблиц коэффициентов для следующих стохастических численных методов:
    - численный метод для скалярного СДУ с сильной сходимостью p_s = 1.5
    - численный метод для многомерного СДУ и винеровского процесса с сильной сходимостью p_s=1.0
    - численный метод для многомерного СДУ и винеровского процесса со слабой сходимостью p_s = 2.0"""
    def __init__(self, name: str, stage: int, det_order: Union[float, str], stoch_order: Union[float, str]):
        """ Construct the dummy table for abstract stochastic numerical method
        :param name: method's name
        :param stage: method's stage
        :param det_order: method's deterministic order
        :param stoch_order: method's stochastic order
        """
        self.name = name
        self.s = stage
        self.pd = float(det_order)
        self.ps = float(stoch_order)
        self.description = 'Метод {0}'.format(self.name)

    def __str__(self):
        return self.description

    @classmethod
    def from_json(cls, file_path: str) -> list:
        """Create coefficients table from JSON file with list of objects"""
        with open(file_path, mode='r', encoding='utf-8') as file:
            data = json.load(file)
        tables = cls.from_dictionary_list(data)
        return tables

    @classmethod
    def from_dictionary_list(cls, dic_list: list) -> list:
        """Create coefficients table from list of python dictionaries"""
        tables = []
        for table in dic_list:
            table = cls.from_dictionary(table)
            tables.append(table)
        return tables

    @classmethod
    def from_dictionary(cls, table: dict):
        """Create coefficients table from single python dictionary. Shell be overloaded
        in child classes
        :param table: dict with info about method"""
        method = cls(table['name'], table['stage'], table['det_order'], table['stoch_order'])
        method.description = table['description']
        return method


class ScalarMethod(CoefficientsTable):
    """Таблица коэффициентов численного метода для случая скалярного СДУ с сильной сходимостью p_s = 1.5"""
    def __init__(self, name: str, stage: int, det_order: Union[float, str], stoch_order: Union[float, str]):
        """
        Construct the table for scalar stochastic numerical method
        :param name: method's name
        :param stage: method's stage
        :param det_order: method's deterministic order
        :param stoch_order: method's stochastic order
        """
        super().__init__(name, stage, det_order, stoch_order)
        self.A0 = np.zeros(shape=(self.s, self.s))
        self.A1 = np.zeros(shape=(self.s, self.s))
        self.B0 = np.zeros(shape=(self.s, self.s))
        self.B1 = np.zeros(shape=(self.s, self.s))
        self.c0 = np.zeros(self.s)
        self.c1 = np.zeros(self.s)
        self.a = np.zeros(self.s)
        self.b1 = np.zeros(self.s)
        self.b2 = np.zeros(self.s)
        self.b3 = np.zeros(self.s)
        self.b4 = np.zeros(self.s)

    @classmethod
    def from_dictionary(cls, table: dict):
        """Create coefficients table from single python dictionary"""
        method = cls(table['name'], table['stage'], table['det_order'], table['stoch_order'])
        method.description = table['description']
        method.A0 = array_from_list(table['A0'])
        method.A1 = array_from_list(table['A1'])
        method.B0 = array_from_list(table['B0'])
        method.B1 = array_from_list(table['B1'])
        method.c0 = array_from_list(table['c0'])
        method.c1 = array_from_list(table['c1'])
        method.a = array_from_list(table['a'])
        method.b1 = array_from_list(table['b1'])
        method.b2 = array_from_list(table['b2'])
        method.b3 = array_from_list(table['b3'])
        method.b4 = array_from_list(table['b4'])

        return method

    def latex_table(self, compact=True):
        """Print coefficients table in LaTeX array format"""
        res = '{0}\n'.format(self.description)
        # example: {c|cccc|cccc|cccc} s = 4
        res += '\\begin{{array}}{{{0}}}\n'.format('c|' + '|'.join(['c'*self.s]*3))
        A0 = fractions_list_to_latex(self.A0, compact)
        A1 = fractions_list_to_latex(self.A1, compact)
        B0 = fractions_list_to_latex(self.B0, compact)
        B1 = fractions_list_to_latex(self.B1, compact)
        c0 = fractions_list_to_latex(self.c0, compact)
        c1 = fractions_list_to_latex(self.c1, compact)
        a = fractions_list_to_latex(self.a, compact)
        b1 = fractions_list_to_latex(self.b1, compact)
        b2 = fractions_list_to_latex(self.b2, compact)
        b3 = fractions_list_to_latex(self.b3, compact)
        b4 = fractions_list_to_latex(self.b4, compact)

        for c_, a_, b_ in zip(c0, A0, B0):
            res += '  {0} & '.format(c_)
            res += ' & '.join(['{0}'.format(a_0) for a_0 in a_]) + ' & '
            res += ' & '.join(['{0}'.format(b_0) for b_0 in b_]) + ' & '
            res += ' & '.join(['' for _ in range(self.s)])
            res += '\\\\\n'
        res += '  \\hline\n'

        for c_, a_, b_ in zip(c1, A1, B1):
            res += '  {0} & '.format(c_)
            res += ' & '.join(['{0}'.format(a_0) for a_0 in a_]) + ' & '
            res += ' & '.join(['{0}'.format(b_0) for b_0 in b_]) + ' & '
            res += ' & '.join(['' for _ in range(self.s)])
            res += '\\\\\n'
        res += '  \\hline\n'

        res += '  & '
        res += ' & '.join(['{0}'.format(a_) for a_ in a]) + ' & '
        res += ' & '.join(['{0}'.format(b_) for b_ in b1]) + ' & '
        res += ' & '.join(['{0}'.format(b_) for b_ in b2])
        res += '\\\\\n'
        res += '  \\hline\n'

        res += '  & '
        res += ' & '.join([' ' for _ in range(self.s)]) + ' & '
        res += ' & '.join(['{0}'.format(b_) for b_ in b3]) + ' & '
        res += ' & '.join(['{0}'.format(b_) for b_ in b4])
        res += '\n'
        res += '\\end{array}\n'
        return res


class StrongVectorMethod(CoefficientsTable):
    """Таблица коэффициентов численного метода для случая многомерного СДУ и винеровского процесса с сильной
    сходимостью p_s=1.0"""
    def __init__(self, name: str, stage: int, det_order: Union[float, str], stoch_order: Union[float, str]):
        """
        Construct the table for multidimensional stochastic numerical method
        :param name: method's name
        :param stage: method's stage
        :param det_order: method's deterministic order
        :param stoch_order: method's stochastic order
        """
        super().__init__(name, stage, det_order, stoch_order)
        self.A0 = np.zeros(shape=(self.s, self.s))
        self.A1 = np.zeros(shape=(self.s, self.s))
        self.B0 = np.zeros(shape=(self.s, self.s))
        self.B1 = np.zeros(shape=(self.s, self.s))
        self.c0 = np.zeros(self.s)
        self.c1 = np.zeros(self.s)
        self.a = np.zeros(self.s)
        self.b1 = np.zeros(self.s)
        self.b2 = np.zeros(self.s)

    @classmethod
    def from_dictionary(cls, table: dict):
        """Create coefficients table from single python dictionary"""
        method = cls(table['name'], table['stage'], table['det_order'], table['stoch_order'])
        method.description = table['description']
        method.A0 = array_from_list(table['A0'])
        method.A1 = array_from_list(table['A1'])
        method.B0 = array_from_list(table['B0'])
        method.B1 = array_from_list(table['B1'])
        method.c0 = array_from_list(table['c0'])
        method.c1 = array_from_list(table['c1'])
        method.a = array_from_list(table['a'])
        method.b1 = array_from_list(table['b1'])
        method.b2 = array_from_list(table['b2'])

        return method

    def latex_table(self, compact=True):
        """Print coefficients table in LaTeX array format"""
        res = '{0}\n'.format(self.description)
        # example: {c|cccc|cccc|cccc} s = 4
        res += '\\begin{{array}}{{{0}}}\n'.format('c|' + '|'.join(['c'*self.s]*3))
        A0 = fractions_list_to_latex(self.A0, compact)
        A1 = fractions_list_to_latex(self.A1, compact)
        B0 = fractions_list_to_latex(self.B0, compact)
        B1 = fractions_list_to_latex(self.B1, compact)
        c0 = fractions_list_to_latex(self.c0, compact)
        c1 = fractions_list_to_latex(self.c1, compact)
        a = fractions_list_to_latex(self.a, compact)
        b1 = fractions_list_to_latex(self.b1, compact)
        b2 = fractions_list_to_latex(self.b2, compact)

        for c_, a_, b_ in zip(c0, A0, B0):
            res += '  {0} & '.format(c_)
            res += ' & '.join(['{0}'.format(a_0) for a_0 in a_]) + ' & '
            res += ' & '.join(['{0}'.format(b_0) for b_0 in b_]) + ' & '
            res += ' & '.join(['' for _ in range(self.s)])
            res += '\\\\\n'
        res += '  \\hline\n'

        for c_, a_, b_ in zip(c1, A1, B1):
            res += '  {0} & '.format(c_)
            res += ' & '.join(['{0}'.format(a_0) for a_0 in a_]) + ' & '
            res += ' & '.join(['{0}'.format(b_0) for b_0 in b_]) + ' & '
            res += ' & '.join(['' for _ in range(self.s)])
            res += '\\\\\n'
        res += '  \\hline\n'

        res += '  & '
        res += ' & '.join(['{0}'.format(a_) for a_ in a]) + ' & '
        res += ' & '.join(['{0}'.format(b_) for b_ in b1]) + ' & '
        res += ' & '.join(['{0}'.format(b_) for b_ in b2])
        res += '\\\\\n'
        res += '\\end{array}\n'
        return res


class WeakVectorMethod(CoefficientsTable):
    """Таблица коэффициентов численного метода для случая многомерного СДУ и винеровского процесса с сильной
    сходимостью p_s=1.0"""
    def __init__(self, name: str, stage: int, det_order: Union[float, str], stoch_order: Union[float, str]):
        """Construct the table for multidimensional stochastic numerical method with weak convergence
        :param name: method's name
        :param stage: method's stage
        :param det_order: method's deterministic order
        :param stoch_order: method's stochastic order
        """
        super().__init__(name, stage, det_order, stoch_order)
        self.A0 = np.zeros(shape=(self.s, self.s))
        self.A1 = np.zeros(shape=(self.s, self.s))
        self.A2 = np.zeros(shape=(self.s, self.s))
        self.B0 = np.zeros(shape=(self.s, self.s))
        self.B1 = np.zeros(shape=(self.s, self.s))
        self.B2 = np.zeros(shape=(self.s, self.s))
        self.c0 = np.zeros(self.s)
        self.c1 = np.zeros(self.s)
        self.c2 = np.zeros(self.s)
        self.a = np.zeros(self.s)
        self.b1 = np.zeros(self.s)
        self.b2 = np.zeros(self.s)
        self.b3 = np.zeros(self.s)
        self.b4 = np.zeros(self.s)

    @classmethod
    def from_dictionary(cls, table: dict):
        """Create coefficients table from single python dictionary"""
        method = cls(table['name'], table['stage'], table['det_order'], table['stoch_order'])
        method.description = table['description']
        method.A0 = array_from_list(table['A0'])
        method.A1 = array_from_list(table['A1'])
        method.A2 = array_from_list(table['A2'])
        method.B0 = array_from_list(table['B0'])
        method.B1 = array_from_list(table['B1'])
        method.B2 = array_from_list(table['B2'])
        method.c0 = array_from_list(table['c0'])
        method.c1 = array_from_list(table['c1'])
        method.c2 = array_from_list(table['c2'])
        method.a = array_from_list(table['a'])
        method.b1 = array_from_list(table['b1'])
        method.b2 = array_from_list(table['b2'])
        method.b3 = array_from_list(table['b3'])
        method.b4 = array_from_list(table['b4'])
        return method

    def latex_table(self, compact=True):
        """Print coefficients table in LaTeX array format"""
        res = '{0}\n'.format(self.description)
        # example: {c|cccc|cccc|cccc} s = 4
        res += '\\begin{{array}}{{{0}}}\n'.format('c|' + '|'.join(['c'*self.s]*3))
        A0 = fractions_list_to_latex(self.A0, compact)
        A1 = fractions_list_to_latex(self.A1, compact)
        A2 = fractions_list_to_latex(self.A2, compact)
        B0 = fractions_list_to_latex(self.B0, compact)
        B1 = fractions_list_to_latex(self.B1, compact)
        B2 = fractions_list_to_latex(self.B2, compact)
        c0 = fractions_list_to_latex(self.c0, compact)
        c1 = fractions_list_to_latex(self.c1, compact)
        c2 = fractions_list_to_latex(self.c1, compact)
        a = fractions_list_to_latex(self.a, compact)
        b1 = fractions_list_to_latex(self.b1, compact)
        b2 = fractions_list_to_latex(self.b2, compact)
        b3 = fractions_list_to_latex(self.b3, compact)
        b4 = fractions_list_to_latex(self.b4, compact)

        for c_, a_, b_ in zip(c0, A0, B0):
            res += '  {0} & '.format(c_)
            res += ' & '.join(['{0}'.format(a_0) for a_0 in a_]) + ' & '
            res += ' & '.join(['{0}'.format(b_0) for b_0 in b_]) + ' & '
            res += ' & '.join(['' for _ in range(self.s)])
            res += '\\\\\n'
        res += '  \\hline\n'

        for c_, a_, b_ in zip(c1, A1, B1):
            res += '  {0} & '.format(c_)
            res += ' & '.join(['{0}'.format(a_0) for a_0 in a_]) + ' & '
            res += ' & '.join(['{0}'.format(b_0) for b_0 in b_]) + ' & '
            res += ' & '.join(['' for _ in range(self.s)])
            res += '\\\\\n'
        res += '  \\hline\n'

        for c_, a_, b_ in zip(c2, A2, B2):
            res += '  {0} & '.format(c_)
            res += ' & '.join(['{0}'.format(a_0) for a_0 in a_]) + ' & '
            res += ' & '.join(['{0}'.format(b_0) for b_0 in b_]) + ' & '
            res += ' & '.join(['' for _ in range(self.s)])
            res += '\\\\\n'
        res += '  \\hline\n'

        res += '  & '
        res += ' & '.join(['{0}'.format(a_) for a_ in a]) + ' & '
        res += ' & '.join(['{0}'.format(b_) for b_ in b1]) + ' & '
        res += ' & '.join(['{0}'.format(b_) for b_ in b2])
        res += '\\\\\n'
        res += '\\end{array}\n'
        return res

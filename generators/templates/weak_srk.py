def {{function_name}}{{m}}(f: Callable[[float, np.ndarray], np.ndarray], g: Callable[[float, np.ndarray], np.ndarray], x_0: np.ndarray, wiener: WienerProcess, test_function: Callable[[np.ndarray], bool]=None, keep_only_last: bool=False) -> Union[List[np.ndarray], np.ndarray]:
    """Стохастический метод Рунге-Кутты со слабой сходимостью `ps=2.0`
    {{function_name}}{{m}}(f, g, x_0, wiener, last=False) -> List[ndarray[float]]

    Parameters
    ----------
    f : `function f(t: float, x: ndarray) -> ndarray`
        Векторная функция сноса (вектор сноса).
    g : `function G(t: float, x: ndarray) -> ndarray`
        Матричная функция диффузии (матрица диффузии).
    x_0 : array of float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: ndarray of float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of ndarray of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops.
    """
    # инициализация коэффициентов метода
    {% for i in range(1,stage+1) %}
        {%- for j in range(1, stage+1) if A0[i-1, j-1] != 0 %}
    A0_{{i}}{{j}} = {{'%0.17g'| format(A0[i-1, j-1]|float)}}
        {% endfor %}
        {% for j in range(1, stage+1) if A1[i-1, j-1] != 0 %}
    A1_{{i}}{{j}} = {{'%0.17g'| format(A1[i-1, j-1]|float)}}
        {% endfor %}
        {% for j in range(1, stage+1) if A2[i-1, j-1] != 0 %}
    A2_{{i}}{{j}} = {{'%0.17g'| format(A2[i-1, j-1]|float)}}
        {% endfor %}
        {% for j in range(1, stage+1) if B0[i-1, j-1] != 0 %}
    B0_{{i}}{{j}} = {{'%0.17g'| format(B0[i-1, j-1]|float)}}
        {% endfor %}
        {% for j in range(1, stage+1) if B1[i-1, j-1] != 0 %}
    B1_{{i}}{{j}} = {{'%0.17g'| format(B1[i-1, j-1]|float)}}
        {% endfor %}
        {% for j in range(1, stage+1) if B2[i-1, j-1] != 0 %}
    B2_{{i}}{{j}} = {{'%0.17g'| format(B2[i-1, j-1]|float)}}
        {% endfor %}
    {% endfor %}
    {% for i in range(1,stage+1) if a[i-1] != 0 %}
    a_{{i}} = {{'%0.17g'| format(a[i-1]|float)}}
    {% endfor %}
    {% for i in range(1,stage+1) if c0[i-1] != 0 %}
    c0_{{i}} = {{'%0.17g'| format(c0[i-1]|float)}}
    {% endfor %}
    {% for i in range(1,stage+1) if c1[i-1] != 0 %}
    c1_{{i}} = {{'%0.17g'| format(c1[i-1]|float)}}
    {% endfor %}
    {% for i in range(1,stage+1) if c2[i-1] != 0 %}
    c2_{{i}} = {{'%0.17g'| format(c2[i-1]|float)}}
    {% endfor %}
    {% for i in range(1,stage+1) if b1[i-1] != 0 %}
    b1_{{i}} = {{'%0.17g'| format(b1[i-1]|float)}}
    {% endfor %}
    {% for i in range(1,stage+1) if b2[i-1] != 0 %}
    b2_{{i}} = {{'%0.17g'| format(b2[i-1]|float)}}
    {% endfor %}
    {% for i in range(1,stage+1) if b3[i-1] != 0 %}
    b3_{{i}} = {{'%0.17g'| format(b3[i-1]|float)}}
    {% endfor %}
    {% for i in range(1,stage+1) if b4[i-1] != 0 %}
    b4_{{i}} = {{'%0.17g'| format(b4[i-1]|float)}}
    {% endfor %}
    # Список для сохранения результатов
    xs = []
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt
    sqrth = np.sqrt(h)
    
    if not keep_only_last:
        xs.append(x)
    else:
        pass

    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
    
    # кратные интегралы Ито для векторного процесса
    Ito2 = wiener.ito_integral_2()

    for I1, I2 in zip(wiener.dx, Ito2):
    {% for i in range(1,stage+1) %}
        X0_{{i}} = x
        {%- for j in range(1,stage+1) if A0[i-1, j-1] != 0 -%}
            {%- if loop.first %} + h * ({% endif -%}
            A0_{{i}}{{j}} * f(t0 {%- if c0[j-1] != 0 %} + c0_{{j}} * h{%- endif -%}, X0_{{j}}){% if not loop.last %} + {% endif %}
            {%- if loop.last %}){% endif -%}
        {%- endfor -%}
        {%- for l in range(1,m+1) -%}
            {%- for j in range(1,stage+1)  if B0[i-1, j-1] != 0 -%}
                {%- if loop.first %} + {% endif -%}
                B0_{{i}}{{j}} * g(t0 {%- if c1[j-1] != 0 %} + c1_{{j}} * h {%- endif -%}, X{{l}}_{{j}})[:, {{l-1}}] * I1[{{l-1}}]{% if not loop.last %} + {% endif %}
            {%- endfor %}
        {% endfor %}

        {% for k in range(1,m+1) %}
        X{{k}}_{{i}} = x 
            {%- for j in range(1,stage+1) if A1[i-1, j-1] != 0 -%}
                {%- if loop.first %} + h * ({% endif -%}
                A1_{{i}}{{j}} * f(t0 {%- if c0[j-1] != 0 %}+ c0_{{j}} * h{%- endif -%}, X0_{{j}}){% if not loop.last %} + {% endif %}
                {%- if loop.last %}){% endif -%}
            {%- endfor %}
            {%- for j in range(1,stage+1) if B1[i-1, j-1] != 0 -%}
                {%- if loop.first %} + {% endif -%}
                B1_{{i}}{{j}} * g(t0 {%- if c1[j-1] != 0 %} + c1_{{j}} * h {%- endif -%}, X{{k}}_{{j}})[:, {{k-1}}] * sqrth {%- if not loop.last %} + {% endif %}
            {%- endfor %}
            
        {% endfor %}
        
        {% for k in range(1,m+1) %}
        Xhat{{k}}_{{i}} = x 
            {%- for j in range(1,stage+1) if A2[i-1, j-1] != 0 -%}
                {%- if loop.first %} + h * ({% endif -%}
                A2_{{i}}{{j}} * f(t0 {%- if c0[j-1] != 0 %}+ c0_{{j}} * h{%- endif -%}, X0_{{j}}){% if not loop.last %} + {% endif %}
                {%- if loop.last %}){% endif -%}
            {%- endfor %}
            {%- for j in range(1,stage+1) if B2[i-1, j-1] != 0 -%}
                {%- for l in range(1,m+1) if l != k -%}
                    {%- if loop.first %} + {% endif -%}
                    B2_{{i}}{{j}} * g(t0 {%- if c1[j-1] != 0 %} + c1_{{j}} * h {%- endif -%}, X{{l}}_{{j}})[:, {{l-1}}] * I2[{{l}}, {{k}}] / sqrth {%- if not loop.last %} + {% endif %}
                {%- endfor %}
            {% endfor %}
            
        {% endfor %}
    {% endfor %}

        x = x
        {%- for i in range(1,stage+1) if a[i-1] != 0 -%}
          {%- if loop.first %} + h * ({% endif -%}
          a_{{i}} * f(t0 {%- if c0[i-1] != 0 %} + c0_{{i}} * h{%- endif -%}, X0_{{i}}){% if not loop.last %} + {% endif %}
          {%- if loop.last %}) {% endif -%}
        {%- endfor -%}
        + 
        {%- for k in range(1,m+1) -%}
          {%- for i in range(1, stage+1) -%}
          {%- if b1[i-1] != 0 or b2[i-1] != 0 -%} ( {%- endif -%}
          {%- if b1[i-1] != 0 -%}
            b1_{{i}} * I1[{{k-1}}]
          {%- endif -%}
          {%- if b2[i-1] != 0 -%}
             {%- if b1[i-1] != 0 -%} + {%- endif -%} b2_{{i}} * I2[{{k}}, {{k}}] / sqrth
          {%- endif -%}
          {%- if b1[i-1] != 0 or b2[i-1] != 0 -%} ) * {% endif -%}
          g(t0{%- if c1[i-1] != 0 %} + c1_{{i}} * h{%- endif -%}, X{{k}}_{{i}})[:, {{k-1}}]{% if not loop.last %} + {% endif -%}
          {% endfor -%}
          {% if not loop.last %} + {% endif -%}
        {% endfor -%}
        + 
        {%- for k in range(1,m+1) -%}
          {%- for i in range(1, stage+1) -%}
          {%- if b3[i-1] != 0 or b4[i-1] != 0 -%} ( {%- endif -%}
          {%- if b3[i-1] != 0 -%}
            b3_{{i}} * I1[{{k-1}}]
          {%- endif -%}
          {%- if b4[i-1] != 0 -%}
             {%- if b3[i-1] != 0 -%} + {%- endif -%} b4_{{i}} * sqrth
          {%- endif -%}
          {%- if b3[i-1] != 0 or b4[i-1] != 0 -%} ) * {% endif -%}
          g(t0{%- if c2[i-1] != 0 %} + c2_{{i}} * h{%- endif -%}, Xhat{{k}}_{{i}})[:, {{k-1}}]{% if not loop.last %} + {% endif -%}
          {% endfor -%}
          {% if not loop.last %} + {% endif -%}
        {% endfor %}
        
        t0 = t0 + h

        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
        if not keep_only_last:
            xs.append(x)
        else:
            pass

    if keep_only_last:
        return x
    else:
        return xs
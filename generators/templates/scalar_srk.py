def {{function_name}}(f: Callable[[float, float], float], g: Callable[[float, float], float], x_0: float, wiener: WienerProcess, test_function: Callable[[float], bool]=None, keep_only_last=False) -> Union[float, List[float]]:
    """Численный метод для скалярного СДУ
    
    Parameters
    ----------
    f : `function f(t, x) -> float`
        Скалярная функция сноса (вектор сноса для скалярного случая).
    g : `function G(t, x) -> float`
        Функция диффузии (матрица диффузии для скалярного случая).
    x_0 : float
        Начальная точка.
    wiener : WienerProcess
        Ведущий (driving) винеровский процесс в виде объекта, который
        следует создать до вызова функции.
    test_function : function `test(x: float) -> bool`
        Функция, которая проверят вычисленные значения `x_n` на физический
        смысл, например >=0 в случае хищника-жертвы. В случае, если она
        возвращает `False` будет выброшена ошибка ValueError.
    keep_only_last : bool
        Сохранять только последнее вычисленное значение или все значения.

    Returns
    -------
    - If `keep_only_last = True` return float
    - If `keep_only_last = False` return list of float

    Raises
    ------
    ValueError
        If `test_function` return `False` the physical meaning
        is lost and the computation stops."""
    # Список для сохранения результатов
    xs = []
    # кратные интегралы Ито для скалярного процесса
    ito1, ito10, ito11, ito111 = wiener.scalar_ito_integrals()
    x = x_0
    t0 = wiener.t_0
    h = wiener.dt
    # инициализация коэффициентов метода
    {% for i in range(1,stage+1) %}
        {%- for j in range(1, stage+1) if A0[i-1, j-1] != 0 %}
    A0_{{i}}{{j}} = {{'%0.17g'| format(A0[i-1, j-1]|float)}}
        {% endfor %}
        {% for j in range(1, stage+1) if A1[i-1, j-1] != 0 %}
    A1_{{i}}{{j}} = {{'%0.17g'| format(A1[i-1, j-1]|float)}}
        {% endfor %}
        {% for j in range(1, stage+1) if B0[i-1, j-1] != 0 %}
    B0_{{i}}{{j}} = {{'%0.17g'| format(B0[i-1, j-1]|float)}}
        {% endfor %}
        {% for j in range(1, stage+1) if B1[i-1, j-1] != 0 %}
    B1_{{i}}{{j}} = {{'%0.17g'| format(B1[i-1, j-1]|float)}}
        {% endfor %}
    {% endfor %}
    {% for i in range(1,stage+1) if a[i-1] != 0 %}
    a_{{i}} = {{'%0.17g'| format(a[i-1]|float)}}
    {% endfor %}
    {% for i in range(1,stage+1) if c0[i-1] != 0 %}
    c0_{{i}} = {{'%0.17g'| format(c0[i-1]|float)}}
    {% endfor %}
    {% for i in range(1,stage+1) if c1[i-1] != 0 %}
    c1_{{i}} = {{'%0.17g'| format(c1[i-1]|float)}}
    {% endfor %}
    {% for i in range(1,stage+1) if b1[i-1] != 0 %}
    b1_{{i}} = {{'%0.17g'| format(b1[i-1]|float)}}
    {% endfor %}
    {% for i in range(1,stage+1) if b2[i-1] != 0 %}
    b2_{{i}} = {{'%0.17g'| format(b2[i-1]|float)}}
    {% endfor %}
    {% for i in range(1,stage+1) if b3[i-1] != 0 %}
    b3_{{i}} = {{'%0.17g'| format(b3[i-1]|float)}}
    {% endfor %}
    {% for i in range(1,stage+1) if b4[i-1] != 0 %}
    b4_{{i}} = {{'%0.17g'| format(b4[i-1]|float)}}
    {% endfor %}
    # Включаем начальную точку в результат
    if not keep_only_last:
        xs.append(x)
    
    # проверяем, передана ли функция для проверки корректности решения,
    # если нет, то делаем функцию-пустышку, возвращающую только True
    if not isinstance(test_function, Callable):
        def test_function(v):
            return False
    else:
        pass
        
    for i1, i10, i11, i111 in zip(ito1, ito10, ito11, ito111):
        # шаг метода
        {% for i in range(1,stage+1) %}
        X0_{{i}} = x 
            {%- for j in range(1,stage+1) if A0[i-1,j-1] != 0 -%}
                {# Скобка открывается #}
                {%- if loop.first %} + h*({% endif -%}
                A0_{{i}}{{j}} * f(t0 {%- if c0[j-1] != 0 %} + c0_{{j}} * h{% endif %}, X0_{{j}}){% if not loop.last %} + {% endif %}
                {# Скобка закрывается #}
                {%- if loop.last %}){% endif -%}
            {%- endfor -%}
            {%- for j in range(1,stage+1) if B0[i-1,j-1] != 0 -%}
                {# Скобка открывается #}
                {%- if loop.first %} + (i10 / np.sqrt(h)) * ({% endif -%}
                B0_{{i}}{{j}} * g(t0 {%- if c1[j-1] != 0 %} + c1_{{j}} * h{% endif %}, X1_{{j}}){% if not loop.last %} + {% endif %}
                {# Скобка закрывается #}
                {%- if loop.last %}){% endif -%}
            {%- endfor %}
            
        X1_{{i}} = x
            {%- for j in range(1,stage+1)  if A1[i-1,j-1] != 0 -%}
                {# Скобка открывается #}
                {%- if loop.first %} + h*({% endif -%}
                A1_{{i}}{{j}} * f(t0 {%- if c0[j-1] != 0 %} + c0_{{j}} * h{% endif %}, X0_{{j}}){% if not loop.last %} + {% endif %}
                {# Скобка закрывается #}
                {%- if loop.last %}){% endif -%}
            {%- endfor %}
            {%- for j in range(1,stage+1)  if B1[i-1,j-1] != 0 -%}
                {# Скобка открывается #}
                {%- if loop.first %} + np.sqrt(h) * ({% endif -%}
                B1_{{i}}{{j}} * g(t0 {%- if c1[j-1] != 0 %} + c1_{{j}} * h{% endif %}, X1_{{j}}){% if not loop.last %} + {% endif %}
                {# Скобка закрывается #}
                {%- if loop.last %}){% endif -%}
            {%- endfor %}
            
        {% endfor %}

        x = x
        {%- for i in range(1,stage+1) if a[i-1] != 0 -%}
            {# Скобка открывается #}
            {%- if loop.first %} + h * ({% endif -%}
            a_{{i}} * f(t0 {%- if c0[i-1] != 0 %} + c0_{{i}} * h{% endif %}, X0_{{i}}){% if not loop.last %} + {% endif %}
            {# Скобка закрывается #}
            {%- if loop.last %}){% endif -%}
        {%- endfor -%}
        + 
        {%- for i in range(1, stage+1) -%} (
            {%- if b1[i-1] != 0 -%}
              b1_{{i}} * i1 {%- if b2[i-1] != 0 -%}+ {% endif -%}
            {%- endif -%}
            {%- if b2[i-1] != 0 -%}
               b2_{{i}} * i11 / np.sqrt(h) {%- if b3[i-1] != 0 -%}+ {% endif -%}
            {%- endif -%}
            {%- if b3[i-1] != 0 -%}
               b3_{{i}} * i10 / h {%- if b4[i-1] != 0 -%}+ {% endif -%}
            {%- endif -%}
            {%- if b4[i-1] != 0 -%}
               b4_{{i}} * i111 / h
            {%- endif -%}) * g(t0 {%- if c1[i-1] != 0 %} + c1_{{i}} * h{% endif %}, X1_{{i}}){% if not loop.last %} + {% endif %}
        {% endfor %}
        
        t0 = t0 + h
        
        # Проверка на адекватность решения
        if test_function(x):
            msg = 'The function `{0}` returned `False` value '.format(test_function.__name__)
            msg += 'and therefore calculation is stopped at time point {0}.'.format(t0)
            raise ValueError(msg)
        else:
            pass
            
        if not keep_only_last:
            xs.append(x)
        else:
            pass
    if keep_only_last:
        return x
    else:
        return xs
